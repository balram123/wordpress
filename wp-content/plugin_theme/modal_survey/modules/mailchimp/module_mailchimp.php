<?php
	if( ! class_exists( 'MailChimp' ) ) {
		require_once( sprintf( "%s/MailChimp.php", dirname( __FILE__ ) ) );
	}
	$MailChimp = new MailChimp( $sopts[ 75 ] );
	$mlid = $mailchimp_listid;
	$result = $MailChimp->call('lists/subscribe', array(
		'id'                => $mlid,
		'email'             => array('email'=>$email),
		'merge_vars'        => $mv,
		'double_optin'      => false,
		'update_existing'   => true,
		'replace_interests' => true,
		'send_welcome'      => true,
	));
	if ( isset( $result['leid'] ) ) {
		if ($result['leid']>0) $result = true;
		else {
			$error_msg = $result['error'];
			$result = false;
		}
	}
	else {
			$result = false;
	}
?>