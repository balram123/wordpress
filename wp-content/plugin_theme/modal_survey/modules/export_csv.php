<?php
if (function_exists('fputcsv')) {
	$path = str_replace("/modules","",sprintf("%s/exports/".$survey_exp['id'].".csv", dirname(__FILE__)));
	$fp = fopen($path, 'w');
		$survey_csv[] = array('"' . __( 'Survey ID', MODAL_SURVEY_TEXT_DOMAIN ) . '"','"' . __( 'Survey Name', MODAL_SURVEY_TEXT_DOMAIN ) . '"','"' . __( 'Generated', MODAL_SURVEY_TEXT_DOMAIN ) . '"');
		$survey_csv[] = array('"'.$survey_exp['id'].'"', '"'.$survey_exp['name'].'"', '"'.$survey_exp['export_time'].'"');
		if ( $personal ) {
			$survey_csv[] = array();
			$survey_csv[] = array( '"' . __( 'User ID', MODAL_SURVEY_TEXT_DOMAIN ) . '"','"' . __( 'Username', MODAL_SURVEY_TEXT_DOMAIN ) . '"','"' . __( 'Created', MODAL_SURVEY_TEXT_DOMAIN ) . '"');
			$survey_csv[] = array( '"' . $survey_exp[ 'user_details' ]->autoid . '"', '"' . ( $survey_exp[ 'user_details' ]->username ? $survey_exp[ 'user_details' ]->username : __( 'Not Specified', MODAL_SURVEY_TEXT_DOMAIN ) ) . '"', '"' . $survey_exp[ 'user_details' ]->created . '"');
			$survey_csv[] = array( '"' . __( 'Email', MODAL_SURVEY_TEXT_DOMAIN ) . '"','"' . __( 'Name', MODAL_SURVEY_TEXT_DOMAIN ) . '"','""');
			$survey_csv[] = array( '"' . $survey_exp[ 'user_details' ]->email . '"', '"' . $survey_exp[ 'user_details' ]->name . '"', '""');
			$survey_csv[] = array( '"' . __( 'Participant answers marked with stars: *', MODAL_SURVEY_TEXT_DOMAIN ) . '"','""','""');
			$survey_csv[] = array();
		}
		$survey_csv[] = array();
		$survey_csv[] = array('"' . __( 'Question / Answer', MODAL_SURVEY_TEXT_DOMAIN ) . '"','"' . __( 'Votes', MODAL_SURVEY_TEXT_DOMAIN ) . '"', '"' . __( 'Percentage', MODAL_SURVEY_TEXT_DOMAIN ) . '"');
						
		foreach ( $survey_exp[ 'questions' ] as $qkey=> $questions ) {
			$survey_csv[] = array('"'.$questions['name'].'"','','');
			foreach ($questions as $key=>$answer) {
				if ( is_numeric( $key ) ) {
					$marker = "";
					if ( $personal ) {
						if ( in_array( $key, $user_votes[ $qkey ] ) ) {
							$marker = "* ";
						}
					}
					$survey_csv[] = array( '"' . $marker . $answer[ 'answer' ] . '"','"' . $answer[ 'count' ] . '"','"' . $answer[ 'percentage' ].'%"' );
				}
			}
					$survey_csv[] = array( '"' . __( 'Total Votes', MODAL_SURVEY_TEXT_DOMAIN ) . '"','"' . $questions['count'] . '"' );
					$survey_csv[] = array( '' );
		}

	foreach ($survey_csv as $fields) {
		fputcsv( $fp, $fields, ',', chr( 0 ) );
	}
	fclose( $fp );
	$result = "success";
}
else {
	$result = __( 'fputcsv function doesn\'t exists', MODAL_SURVEY_TEXT_DOMAIN );
}
?>