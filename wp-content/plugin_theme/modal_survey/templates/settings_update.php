	<div id="screen_preloader" style="position: absolute;width: 100%;height: 1000px;z-index: 9999;text-align: center;background: #fff;padding-top: 200px;"><h3>Modal Survey for WordPress</h3><img src="<?php print(plugins_url( '/assets/img/screen_preloader.gif' , __FILE__ ));?>"><h5><?php _e( 'LOADING', MODAL_SURVEY_TEXT_DOMAIN );?><br><br><?php _e( 'Please wait...', MODAL_SURVEY_TEXT_DOMAIN );?></h5></div>
	<div class="wrap" style="visibility:hidden">
<div class="wrap" style="visibility:hidden">
	<br />
	<h3>Modal Survey - <?php _e( 'Update', MODAL_SURVEY_TEXT_DOMAIN );?></h3>
	<div class="help_link"><a target="_blank" href="http://modalsurvey.pantherius.com/documentation/#line7"><?php _e( 'Documentation', MODAL_SURVEY_TEXT_DOMAIN );?></a></div>
	<hr />
	<?php 
		require_once(str_replace('templates','',sprintf("%s/modules/manual.update.php", dirname(__FILE__))));
		manual_plugin_updater::getInstance(
		'modal_survey/modal_survey.php',
		'modal_survey/modal_survey.php',
		array(),
		'modal_survey'
		);
	?>
</div>