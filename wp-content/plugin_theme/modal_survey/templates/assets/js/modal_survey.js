;(function ( $, window, document, undefined ) {
	/** Create the defaults once **/
	var pluginName = "modalsurvey",
			defaults = {
			survey_options 	: [],
			unique_key		: ''
	};
/** The actual plugin constructor **/
function Plugin ( element, options ) {
	this.element = element;
	this.settings = $.extend( {}, defaults, options );
	this._defaults = defaults;
	this._name = pluginName;
	this.init();
}

/** Avoid Plugin.prototype conflicts **/
$.extend( Plugin.prototype, {
		init: function () {
		var survey = [], unique_key = this.settings.unique_key, survey_options = [], played_question = -1, rmdni = false, lastScrollTop = 0, played = 0, sanswers = [], msparams = [], timer, pos, survey_aoptions = [], survey_qoptions = [], opentext = "", surveycatscore = new Array(), surveyscore = 0, surveycorrect = 0, chtitle, question_score = new Array(), question_correct = new Array(), question_name = new Array(), question_choice = new Array(),chartparams = [],chartelems = {}, random, charttype, chartmode, socialbuttons, socialstyle = "", socialsize = "", survey_participant_form = "", pformstep, endstep, sendbutton, senderror = false, inputtemp, noclose = "false", between = [], soctitle = "", socdesc = "", socimg = "", rating_selected = [], alldisplay_survey_content = "", currenthover, imgwidth, imgheight, imgstyle;
		if ( this.settings.survey_options != undefined ) survey = JSON.parse( this.settings.survey_options );
		if ( survey.options != undefined ) survey_options = JSON.parse( survey.options );
		//if ( survey.social != undefined ) survey_social = JSON.parse( survey.social );
		if ( survey.ao != undefined ) survey_aoptions = survey.ao;
		if ( survey.qo != undefined ) survey_qoptions = survey.qo;
		if ( survey.align == undefined || survey.align == "" ) survey.align = "left";
		if ( survey.visible == undefined || survey.visible == "" ) survey.visible = "false";
		if ( survey.width == undefined ) survey.width = "100%";
		if ( survey.textalign == undefined ) survey.textalign = "";
		if ( survey.display_once != '' && survey.display_once != "undefined" ) {
				if ( survey_options[ 143 ] == '' ) {
					survey_options[ 143 ] = 99999;
				}
				msparams = [ 'modal_survey', survey.display_once, parseFloat( survey_options[ 143 ] ) * 60, 'minutes' ];
				setCookie( msparams );
		}
		if ( survey_options[ 21 ] != "" ) {
			survey.survey_conds = jQuery( survey_options[ 21 ] ).toArray();
		}
		else {
			survey.survey_conds = "";
		}
		if ( survey_options[ 23 ] == undefined ) {
			survey_options[ 23 ] = 5000;
		}
		if ( survey_options[ 133 ] == undefined || survey_options[ 133 ] == "" ) {
			survey_options[ 133 ] = "preloader";
		}
		if ( survey_options[ 134 ] == undefined || survey_options[ 134 ] == "" ) {
			survey_options[ 134 ] = "";
		}
		if ( survey_options[ 135 ] == undefined || survey_options[ 135 ] == "" ) {
			survey_options[ 135 ] = 1000;
		}
		if ( survey_options[ 145 ] == undefined || survey_options[ 145 ] == "" ) {
			survey_options[ 145 ] = "remove";
		}
		if ( survey.social[ 0 ] == 'on' ) {
			if ( survey.social[ 2 ].indexOf( 'large' ) >= 0 ) {
				socialsize = " is-large";
			}
			if ( survey.social[ 2 ].indexOf( 'clean' ) >= 0  ) {
				socialstyle = " is-clean";
			}
			socialbuttons = '<div class="social-sharing' + socialstyle + socialsize + '">';
				jQuery.map( survey.social[ 1 ], function( val, i ) {
					if ( val == "facebook" ) {
						socialbuttons += '<a target="_blank" href="http://www.facebook.com/sharer.php?u=' + survey.social[ 4 ] + '" class="share-facebook ms-social-share"><span class="icon icon-facebook" aria-hidden="true"></span><span class="share-title">Share</span></a>';
					}
					if ( val == "twitter" ) {
						socialbuttons += '<a target="_blank" href="http://twitter.com/share?url=' + survey.social[ 4 ] + '&amp;text=' + survey.social[ 6 ] + '" class="share-twitter ms-social-share"><span class="icon icon-twitter" aria-hidden="true"></span><span class="share-title">Tweet</span></a>';
					}
					if ( val == "pinterest" ) {
						socialbuttons += '<a target="_blank" href="http://pinterest.com/pin/create/button/?url=' + survey.social[ 4 ] + '&amp;media=' + survey.social[ 5 ] + '&amp;description=' + survey.social[ 6 ] + '" class="share-pinterest ms-social-share"><span class="icon icon-pinterest" aria-hidden="true"></span><span class="share-title">Pin it</span></a>';
					}
					if ( val == "googleplus" ) {
						socialbuttons += '<a target="_blank" href="http://plus.google.com/share?url=' + survey.social[ 4 ] + '" class="share-google ms-social-share"><span class="icon icon-google" aria-hidden="true"></span><span class="share-title">+1</span></a>';
					}
					if ( val == "linkedin" ) {
						socialbuttons += '<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=' + survey.social[ 4 ] + '&title=' + survey.social[ 6 ] + '&summary=' + survey.social[ 7 ] + '" class="share-linkedin ms-social-share"><span class="icon icon-linkedin" aria-hidden="true"></span><span class="share-title">Share</span></a>';
					}
				});
			socialbuttons += '</div>';
		}
		else {
			socialbuttons = "";
		}
		var modal_survey = jQuery( "#survey-" + survey.survey_id + "-" + unique_key );
		//bbPress Extension
		if ( jQuery( "#bbpress-forums" ).length > 0 ) {
			modal_survey.css( "display", "none" );
			modal_survey.prependTo( "#bbpress-forums" );
			modal_survey.addClass( "bbpress-modal-survey" );
			modal_survey.slideDown( parseInt( survey_options[ 11 ] ), survey_options[ 1 ], function() {} );
		}
		if ( survey.style != 'flat' ) {
			survey.num_width = survey.width.replace("%","");
			survey.leftmargin = "0";
			if ( survey.align == "center" && survey.num_width < 100 ) {
				survey.leftmargin = ( 100 - survey.num_width ) / 2;
			}
			modal_survey.attr( "style", "position: fixed;" + survey.align + ":0px;z-index: 999999;width:" + survey.width + ";margin-left: " + survey.leftmargin + "%;bottom:-300px;" ); 
			modal_survey.prependTo("body");
			modal_survey.removeClass("modal-survey-embed");
		}
		function detectmob() {
		   if( window.innerWidth <= 800 && window.innerHeight <= 600 ) {
			 return true;
		   } else {
			 return false;
		   }
		}
		  
		jQuery("body").on( "click", "." + survey.survey_id, function( e ) {
			e.preventDefault();
			played_question = 0;
			if ( parseInt( survey_options[13] ) == 1 ) jQuery( "#bglock" ).fadeIn( 1000 );
			modal_survey.css( "z-index", "999999" );
			play_survey();
		})
		if ( parseInt( survey_options[ 13 ] ) == 1 && survey.style != 'flat' ) {
			if ( jQuery( "#bglock" ).length == 0 ) jQuery( "body" ).prepend( "<div id='bglock'></div>" );
			if ( ( survey.expired != 'true' ) && ( survey.style != 'click' ) && ( survey_options[ 15 ] != 1 ) ) jQuery( "#bglock" ).fadeIn( 1000 );
		}
		if ( survey_options[ 15 ] != 1 && survey.style == 'modal' ) {
			if ( survey.preview == "true" ) {
				setTimeout( function() {
					played_question = 0;
					play_survey();
				}, 0 );
			}
			else {
				setTimeout( function() {
					played_question = 0;
					play_survey();
				}, survey_options[ 135 ] );				
			}
		}
		if ( survey.style == 'flat' ) {
			if ( survey.visible == 'false' ) {
				modal_survey.hide();
			}
		}
		if ( survey.style == 'flat' && survey.visible == 'true' ) {
			played_question = 0;
			play_survey();
		}
		jQuery( window ).bind( 'scroll' , function () {
			if ( modal_survey != undefined ) {
				if ( played == 0 ) {
					if ( survey.style == 'flat' && played_question == -1 && modal_survey.visible() ) {
						played_question = 0;
						play_survey();
					}
					else
					{
						if ( jQuery( window ).scrollTop() + jQuery( window ).height() > jQuery( document ).height() - ( ( jQuery( document ).height() / 100 ) * 10 ) && jQuery( this ).scrollTop() > lastScrollTop && played_question == -1 ) {
							if ( survey.style == 'modal' && survey_options[ 15 ] == 1 ) {
								if ( parseInt( survey_options[ 13 ] ) == 1 ) {
									jQuery( "#bglock" ).fadeIn( 1000 );
								}
								played_question = 0;
								play_survey();
							}
						}
						parallaxScroll();
						clearTimeout( timer );
						if ( played_question >= 0 ) {
							timer = setTimeout( refresh , 150 );
						}
					}
				}
			}
		});
		var refresh = function () {
			if ( played_question >= 0 ) {	
				if ( survey_options[ 0 ] == "bottom" ) {
					jQuery( modal_survey ).animate({ bottom: '10px' }, parseInt( survey_options[ 11 ] ), 'easeOutBounce' );
				}
				if ( survey_options[ 0 ] == "center" ) {
					modal_survey.animate({ top: ( ( jQuery( window ).height() - modal_survey.height() ) / 2 ) + "px" }, parseInt( survey_options[ 11 ] ), 'easeOutBounce' );
				}
				if ( survey_options[ 0 ] == "top" ) {
					modal_survey.animate({ top: '10px' }, parseInt( survey_options[ 11 ] ), 'easeOutBounce' );
				}
			}
		}
		function parallaxScroll() {
			var scrolledY = jQuery( window ).scrollTop();
			if ( jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .survey_element" ).length ) {
				if ( survey_options[ 0 ] == "center" ) {
					pos = "top";
				}
				else {
					pos = survey_options[0];
				}
				if ( scrolledY < lastScrollTop ) {
					modal_survey.css( pos, parseInt( modal_survey.css( pos ).replace( "px", "" ) ) - parseInt( scrolledY / 300 ) + "px" );
				}
				else
				{
					modal_survey.css( pos, parseInt( modal_survey.css( pos ).replace( "px", "" ) ) + parseInt( scrolledY / 300 ) + "px" );
				}
			}
			lastScrollTop = jQuery( window ).scrollTop();
		}
			
		function isUrlValid( url ) {
			return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
		}

		function countTo( element, from, to, speed, refreshInterval, afterString, decimals, onUpdate, onComplete) {
		var loops = Math.ceil( parseInt( speed ) / parseInt( refreshInterval ) ),
			element = jQuery( element ),
			increment = ( parseInt( to ) - parseInt( from ) ) / parseInt( loops );
			var loopCount = 0,
				value = from,
				interval = setInterval( updateTimer, parseInt( refreshInterval ) );
			function updateTimer() {
				value += increment;
				loopCount++;
				$( element ).html( value.toFixed( decimals ) + afterString );

				if ( typeof(onUpdate) == 'function' ) {
					onUpdate.call( element, value );
				}

				if ( loopCount >= loops ) {
					clearInterval( interval );
					value = to;

					if ( typeof( onComplete ) == 'function' ) {
						onComplete.call( element, value );
					}
				}
			}
		}
		
		function send_answer( thiselem, answer ) {
			if ( survey.display == "all" && thiselem.hasClass( "survey_rating" ) ) {
					return;
			}
			if ( jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .survey_answer_choice img" ).length > 0 ) {
				jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .survey_answer_choice img" ).css( "visibility", "hidden" );
			}
			if ( survey_qoptions[ played_question ][ 3 ] == 1 ) {
				thiselem.parent().append( '<div id="survey_preloader"><img width="20" src="' + survey.plugin_url + '/templates/assets/img/' + survey_options[ 133 ] + '.gif"></div>' );
			}
			else {
				thiselem.append( '<div id="survey_preloader"><img width="20" src="' + survey.plugin_url + '/templates/assets/img/' + survey_options[ 133 ] + '.gif"></div>' );
			}
			var thissurvey = [], qa = {}, thisscore = 0, thiscorrect = 0;
			if ( typeof thiselem.attr( "data-qstep" ) != undefined ) {
				var sans = thiselem.attr( "data-qstep" );
			}
			else {
				var sans = "";
			}
				var regExp = /\[(.*?)\]/;
				var matches = regExp.exec( survey.questions[ played_question ][ 0 ] );
				if ( matches != null ) {
					if ( surveycatscore[ matches[ 1 ] ] == undefined ) {
						surveycatscore[ matches[ 1 ] ] = 0;
					}
				}
				jQuery.map( survey.questions[ played_question ], function( val, i ) {
					if ( i > 0 ) {
						var matches2 = regExp.exec( val );
						if ( matches2 != null ) {
							if ( surveycatscore[ matches2[ 1 ] ] == undefined && ! jQuery.isNumeric( matches2[ 1 ] ) ) {
								surveycatscore[ matches2[ 1 ] ] = 0;
							}
						}
					}
				})
			if( typeof answer === 'string' ) {
				if ( parseInt( survey_aoptions[ parseInt( played_question + 1 ) + "_" + answer ][ 4 ] ) >= 0 ) {
					thisscore = parseInt( survey_aoptions[ parseInt( played_question + 1 ) + "_" + answer ][ 4 ] );
				}
				if ( parseInt( survey_aoptions[ parseInt( played_question + 1 ) + "_" + answer ][ 5 ] ) >= 0 ) {
					thiscorrect = parseInt( survey_aoptions[ parseInt( played_question + 1 ) + "_" + answer ][ 5 ] );
				}
				if ( matches != null ) {
					if ( matches[ 1 ] != undefined ) {
						surveycatscore[ matches[ 1 ] ] = parseInt( surveycatscore[ matches[ 1 ] ] ) + thisscore;
					}
				}
				var matches3 = regExp.exec( survey.questions[ played_question ][ parseInt( answer ) ] );
				if ( matches3 != null ) {
						if ( matches3 != null ) {
							if ( matches3[ 1 ] != undefined && ! jQuery.isNumeric( matches3[ 1 ] ) ) {
								surveycatscore[ matches3[ 1 ] ] = parseInt( surveycatscore[ matches3[ 1 ] ] ) + thisscore;
							}
						}
				}
				surveyscore = parseInt( surveyscore ) + thisscore;
				surveycorrect = parseInt( surveycorrect ) + thiscorrect;
				question_score[ parseInt( played_question + 1 ) ] = thisscore;
				question_correct[ parseInt( played_question + 1 ) ] = thiscorrect;
				question_choice[ parseInt( played_question + 1 ) ] = answer;
			}
			else {
				jQuery.map( answer, function( val, i ) {
				if ( parseInt( survey_aoptions[ parseInt( played_question + 1 ) + "_" + val ][ 4 ] ) >= 0 ) {
					thisscore = parseInt( survey_aoptions[ parseInt( played_question + 1 ) + "_" + val ][ 4 ] );
				}
				else {
					thisscore = 0;
				}
				if ( parseInt( survey_aoptions[ parseInt( played_question + 1 ) + "_" + val ][ 5 ] ) >= 0 ) {
					thiscorrect = parseInt( survey_aoptions[ parseInt( played_question + 1 ) + "_" + val ][ 5 ] );
				}
				else {
					thiscorrect = 0;
				}
				if ( matches != null ) {
					if ( matches[ 1 ] != undefined ) {
						surveycatscore[ matches[ 1 ] ] = parseInt( surveycatscore[ matches[ 1 ] ] ) + thisscore;
					}
				}
				var matches3 = regExp.exec( survey.questions[ played_question ][ parseInt( val ) ] );
				if ( matches3 != null ) {
						if ( matches3 != null ) {
							if ( matches3[ 1 ] != undefined && ! jQuery.isNumeric( matches3[ 1 ] ) ) {
								surveycatscore[ matches3[ 1 ] ] = parseInt( surveycatscore[ matches3[ 1 ] ] ) + thisscore;
							}
						}
				}
					surveyscore = parseInt( surveyscore ) + thisscore;
					surveycorrect = parseInt( surveycorrect ) + thiscorrect;
					question_score[ parseInt( played_question + 1 ) ] = thisscore;
					question_correct[ parseInt( played_question + 1 ) ] = thiscorrect;
					question_choice[ parseInt( played_question + 1 ) ] = answer;
				});
			}
			if ( modal_survey.find( ".open_text_answer" ).length > 0 ) {
				if ( modal_survey.find( ".open_text_answer" ).val() != "" ) {
					if ( modal_survey.find( ".open_text_answer" ).parent().hasClass( survey_options[ 134 ] + "selected" ) ) {
						opentext = modal_survey.find( ".open_text_answer" ).val();
					}
				}
			}
			if ( survey.preview == undefined ) {
				survey.preview = "false";
			}
			qa[ 'sid' ] = survey.survey_id;
			qa[ 'auto_id' ] = survey.auto_id;
			qa[ 'qid' ] = played_question + 1;
			qa[ 'aid' ] = answer;
			qa[ 'open' ] = opentext;
			qa[ 'postid' ] = survey.postid;
			thissurvey.push( qa );
			rmdni = true;
			endcontent = false;
			if ( ( played_question + 1 == endstep ) && ( ( survey_options[ 125 ] != 1 || survey.form == 'false' ) || survey.form != 'true' ) ) {
				endcontent = true;
			}
			var data = {
				action: 'ajax_survey_answer',
				sspcmd: 'save',
				endcontent: endcontent,
				options: JSON.stringify( thissurvey ),
				preview: survey.preview
				};
				jQuery.post( survey.admin_url, data, function( response ) {
					if ( response.toLowerCase().indexOf( "success" ) >= 0 ) {	
						jQuery( "#survey_preloader" ).remove();
						//make animation
						if ( sans != "" ) {
							played_question = parseInt(sans)-1;
						}
						else {
							played_question++;
						}
						continue_survey();
					}
					rmdni = false;
					sanswers = [];
				}).fail(function() {
						jQuery( "#survey_preloader" ).remove();						
						rmdni = false;
						sanswers = [];
				  })
		}
		function array_max( array ) {
			return Object.keys( array ).reduce( function( a, b ){ return array[ a ] > array[ b ] ? a : b } );
		}		 
		function array_min( array ) {
			return Object.keys( array ).reduce( function( a, b ){ return array[ a ] < array[ b ] ? a : b } );
		}
		function continue_survey() {
			if ( survey.style == 'flat' ) {
				modal_survey.slideUp( parseInt( survey_options[ 11 ] ), survey_options[ 1 ], function() {
					play_survey();
					// Animation complete.
				});
			}
			else {
				if ( survey_options[ 0 ] == "bottom" ) {
					modal_survey.animate({ bottom: "-" + parseInt( modal_survey.height() + 100 ) + "px" }, parseInt( survey_options[ 11 ]), survey_options[ 1 ], function(){
						play_survey();
						})
					}
					if ( survey_options[ 0 ] == "center" ) {
						if ( survey.align == "left" ) {
							modal_survey.animate({ left: "-5000px" }, parseInt( survey_options[ 11 ] ), survey_options[ 1 ], function() {
								play_survey();
								});
						}
						else {
							modal_survey.animate({ right: "-5000px" }, parseInt( survey_options[ 11 ] ), survey_options[ 1 ], function() {
								play_survey();
								});
						}
					}
					if ( survey_options[ 0 ] == "top" ) {
						modal_survey.animate({ top: "-" + parseInt( modal_survey.height() + 100 ) + "px" }, parseInt( survey_options[ 11 ] ), survey_options[ 1 ], function(){
							play_survey();
							})
					}
			}
		}
		jQuery( "body" ).on( "click", "#survey-" + survey.survey_id + "-" + unique_key + " .survey_answer_choice", function() {
			if ( rmdni == false ) {
				if ( sanswers.length >= survey_qoptions[ played_question ][ 1 ] ) {
					send_answer( jQuery( this ), sanswers );
				}
				else {
					return;
				}
			}
			else {
				return true;
			}
		})
		jQuery( "body" ).on( "change", "#survey-" + survey.survey_id + "-" + unique_key + " .open_text_answer", function() {
		  opentext = jQuery( this ).val();
		  jQuery( this ).parent().css( "textDecoration", "none" );
		  jQuery( this ).parent().trigger( "click" );
		});
		jQuery( "body" ).on( "click", "#survey-" + survey.survey_id + "-" + unique_key + " .survey_answers", function() {
		var thisanswer = jQuery( this ), pq;
		if ( ! thisanswer.hasClass( ".survey_rating" ) ) {
			if ( thisanswer.attr( "class" ).indexOf( "survey_answer_choice" ) > 0 ) return;
			if ( thisanswer.children( ".open_text_answer" ).length > 0 ) {
				if ( thisanswer.children( ".open_text_answer" ).val().length < 3 ) return;
			}
		}
			if ( survey.display == 'all' ) {
				pq = jQuery( this ).attr( "qid" );
			}
			else {
				pq = played_question;				
			}
			if ( ( survey_qoptions[ pq ][ 0 ] > 1 || survey.questions[ pq ][ 'hasopen' ] == true || survey.display == 'all' ) && ( ! thisanswer.hasClass( "survey_rating" ) && ! thisanswer.hasClass( "ms_rating_question" ) ) ) {
				rmdni = true;
				if ( thisanswer.hasClass( survey_options[ 134 ] + "selected" ) ) {
					thisanswer.removeClass( survey_options[ 134 ] + "selected" );
					thisanswer.children(".open_text_answer" ).removeClass( survey_options[ 134 ] + "selected" );
					if ( jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .survey_answer_choice" ).length > 0 ) {
						jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .survey_answer_choice" ).attr( "data-qstep", "" );
					}
					sanswers = jQuery.grep( sanswers, function( value ) {
					  return value != thisanswer.attr( "id" ).replace( "survey_answer", "" );
					});
				}
				else {
					if ( sanswers.length == survey_qoptions[ pq ][ 0 ] && survey_qoptions[ pq ][ 0 ] > 1 ) {
						return;
					}
					if ( sanswers.length == survey_qoptions[ pq ][ 0 ] && survey_qoptions[ pq ][ 0 ] < 2 ) {
						jQuery( this ).parent().parent().find( ".survey_answers" ).removeClass( survey_options[ 134 ] + "selected" );
					}
					if ( sanswers.length == survey_qoptions[ pq ][ 0 ] && survey.display == 'all' ) {
						sanswers = [];
						jQuery( this ).parent().parent().find( ".survey_answers" ).each( function( index ) {
							if ( jQuery( this ).hasClass( survey_options[ 134 ] + "selected" ) ) {
								sanswers.push( index + 1 );
							}
						})
						if ( sanswers.length == survey_qoptions[ pq ][ 0 ] ) {
							return;
						}
					}
					thisanswer.addClass( survey_options[ 134 ] + "selected" );
					thisanswer.children(".open_text_answer" ).addClass( survey_options[ 134 ] + "selected" );
					if ( jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .survey_answer_choice" ).length > 0 ) {
						jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .survey_answer_choice" ).attr( "data-qstep", thisanswer.attr( "data-qstep" ) );
					}
					sanswers.push( thisanswer.attr( "id" ).replace( "survey_answer", "" ) );
					opentext = thisanswer.children( ".open_text_answer" ).val();
				}
				rmdni = false;
				if ( sanswers.length >= survey_qoptions[ pq ][ 1 ] ) {
					jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .survey_answer_choice" ).children( "img" ).attr( "src", survey.plugin_url + "/templates/assets/img/next.png" );
				}
				else {
					jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .survey_answer_choice" ).children( "img" ).attr( "src", survey.plugin_url + "/templates/assets/img/next-passive.png" );
				}
			}
			else {
				if ( rmdni == false && survey.display != "all" ) {
					sanswers = [ thisanswer.attr( "id" ).replace( "survey_answer", "" ) ];
					send_answer( thisanswer, sanswers );
				}
				else {
					return true;
				}
			}
		})
		function setCookie( params ) {
		var c_name = params[ 0 ], value = params[ 1 ], dduntil = params[ 2 ], mode = params[ 3 ];
			if ( value === undefined ) {
				return true;
			}
			if ( mode == 'days' ) {
				var exdate = new Date();
				exdate.setDate( exdate.getDate() + parseInt( dduntil ) );
				var c_value = escape( value ) + ( ( dduntil == null ) ? "" : "; expires=" + exdate.toUTCString() ) + "; path=/";
				document.cookie = c_name + "=" + c_value;
			}
			if (mode=='minutes') {
				var exdate = new Date();
				exdate.setMinutes( exdate.getMinutes() + parseInt( dduntil ) )
				var c_value = escape( value ) + ( ( dduntil == null ) ? "" : "; expires=" + exdate.toUTCString() ) + "; path=/";
				document.cookie = c_name + "=" + c_value;
			}
		}
		/*
		jQuery( document ).on( "mouseover", "#survey-" + survey.survey_id + "-" + unique_key + " .survey_rating", function() {
			var thisid = parseInt( jQuery( this ).attr( "id" ).replace( "survey_answer", "" ) );
			for(var i = thisid; i > 0; i--){
				modal_survey.find( "#survey_answer" + i ).css( "background-image", "url(" + survey.plugin_url + "/templates/assets/img/star-icon.png)" );
			}
        })
		jQuery( document ).on( "mouseout", ".survey_rating", function() {
				modal_survey.find( ".survey_rating" ).css( "background-image", "url(" + survey.plugin_url + "/templates/assets/img/star-icon-empty.png)" );
       })
	   */
		jQuery( document ).on( "mouseover", "#survey-" + survey.survey_id + "-" + unique_key + " .survey_rating", function() {
			var thisid = parseInt( jQuery( this ).attr( "id" ).replace( "survey_answer", "" ) );
			var parentitem = jQuery( this ).parent().parent();
			currenthover = jQuery( this );
				for( var i = thisid; i > 0; i-- ) {
					if ( parentitem != undefined ) {
						parentitem.find( "#survey_answer" + i ).each( function( index ) {
							jQuery( this ).css( "background-image", "url(" + survey.plugin_url + "/templates/assets/img/star-icon.png)" );
						})
					}
				}
			currenthover.mouseleave( function() {
				if ( rating_selected[ parentitem.attr( "qid" ) ] == undefined ) {
					if ( parentitem != undefined ) {
						parentitem.find( ".survey_rating" ).css( "background-image", "url(" + survey.plugin_url + "/templates/assets/img/star-icon-empty.png)" );
					}
				}
				else {
					for( var i = thisid; i > rating_selected[ parentitem.attr( "qid" ) ]; i-- ) {
						if ( parentitem != undefined ) {
							parentitem.find( "#survey_answer" + i ).each( function( index ) {
								jQuery( this ).css( "background-image", "url(" + survey.plugin_url + "/templates/assets/img/star-icon-empty.png)" );
							})
						}
					}				
				}
			})
	   });  
		jQuery( document ).on( "click", ".survey_rating", function() {
			var thisid = parseInt( jQuery( this ).attr( "id" ).replace( "survey_answer", "" ) );
			var parentitem = jQuery( this ).parent().parent();
			parentitem.find( ".survey_rating" ).removeClass( "selected" );
			jQuery( this ).addClass( "selected" );
			parentitem.find( ".survey_rating" ).css( "background-image", "url(" + survey.plugin_url + "/templates/assets/img/star-icon-empty.png)" );
			rating_selected[ jQuery( this ).attr( "qid" ) ] = thisid;
			for( var i = thisid; i > 0; i-- ) {
					parentitem.find( "#survey_answer" + i ).each( function( index ) {
						jQuery( this ).css( "background-image", "url(" + survey.plugin_url + "/templates/assets/img/star-icon.png)" );
					})
			}
      })
	   
	   function make_animation() {
			if ( survey.style == 'flat' ) {
				modal_survey.slideDown( parseInt( survey_options[ 11 ] ), survey_options[ 1 ], function() {
					display_chart();
					if ( endstep == played_question && survey.display == "all" ) {
						jQuery( "html, body" ).animate({
								scrollTop: modal_survey.offset().top - 200
							}, 200 );					
					}
				});
			}
			else {
				if ( survey_options[ 0 ] == "bottom" ) {
				modal_survey.css( "bottom", "-" + parseInt( modal_survey.height() + 100 ) + "px" );
				modal_survey.animate( { bottom: '10px' }, parseInt( survey_options[ 11 ] ), survey_options[ 1 ], function() {display_chart();} )
				}
				if ( survey_options[ 0 ] == "center" ) {
					if ( survey.align == "left" ) {
						modal_survey.css( "left", "-5000px" );
						modal_survey.css( "top", ( ( jQuery( window ).height() - modal_survey.height() ) / 2 ) + "px" );
						modal_survey.animate({ left: "0px" }, parseInt( survey_options[ 11 ] ), survey_options[ 1 ], function() {display_chart();} );
					}
					else
					{
						modal_survey.css( "right", "-5000px" );
						modal_survey.css( "top", ( ( jQuery( window ).height() - modal_survey.height() ) / 2 ) + "px" );
						modal_survey.animate({ right: "0px" }, parseInt( survey_options[ 11 ] ), survey_options[ 1 ], function() {display_chart();} );
					}
				}
				if ( survey_options[ 0 ] == "top" ) {
					modal_survey.css( "top", "-" + parseInt( modal_survey.height() + 100 ) + "px" );
					modal_survey.animate({ top: '10px' }, parseInt( survey_options[ 11 ] ), survey_options[ 1 ], function() {display_chart();} );
				}
			}
	   }
		 
		function set_survey_style() {
			bgs = survey_options[ 3 ].split(";");
			for ( i = 0; i < bgs.length - 1; ++i ) {
				jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .survey_element" ).css( "background", jQuery.trim( bgs[ i ] ) );
			}
			jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .survey_element" ).css({ 
				"color": survey_options[ 4 ], 
				"border": "solid " + survey_options[ 6 ] + "px " + survey_options[ 5 ], 
				"padding": survey_options[ 9 ] + "px", 
				"font-size": survey_options[ 8 ] + "px", 
				"border-radius": survey_options[ 7 ] + "px"
			});
			jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .survey_question, #survey-" + survey.survey_id + "-" + unique_key + " .survey_answers" ).css({
				"box-shadow": survey_options[ 128 ] + "px " + survey_options[ 129 ] + "px " + survey_options[ 130 ] + "px " + survey_options[ 131 ] + "px " + survey_options[ 132 ]
			});
			jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .survey_rating" ).css({
				"box-shadow": "0px 0px 0px 0px transparent"				
			});
			if ( survey_options[ 16 ] != undefined ) {
				jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .survey_element" ).css( "text-align", survey_options[ 16 ] );
			}
			if ( survey.textalign != "" ) {
				jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .survey_element" ).css( "text-align", survey.textalign );
			}

			if ( survey_options[ 2 ] != "" ) {
				if ( !jQuery( "link[href='http://fonts.googleapis.com/css?family=" + survey_options[ 2 ] + "']" ).length ) {
					jQuery('head').append('<link rel="stylesheet" href="http://fonts.googleapis.com/css?family='+survey_options[2].replace(' ','+').replace(' ','+').replace(' ','+')+':400,700" type="text/css" />');
				}
				jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .survey_element" ).css( "font-family", "'" + survey_options[ 2 ] + "', serif" );
			}			
		}
		 
		function play_survey() {
			var survey_content = "", remove_image = "", percent = 0, percent_last = 0, survey_progress_bar = "", bgs, i, key, qstep, oindex, next_button = "", answerimg = "", qimg = "", tooltip, tableclass;
			opentext = "";
			pformstep = survey.questions.length;
			if ( survey.display == 'all' && endstep > played_question ) {
				survey_content = alldisplay_survey_content;
			}
			if ( survey.display == "all" ) {
				tableclass = "alldisplay";
			}
			else {
				tableclass = "eachdisplay";						
			}
			if ( ( ( survey_options[ 125 ] == 1 && survey.form != 'false' ) || survey.form == 'true' ) && ( survey_options[ 126 ] == 1 || survey_options[ 127 ] == 1 ) ) {
				endstep = pformstep + 1;
			}
			else {
				endstep = survey.questions.length;
			}
			if ( survey_options[ 20 ] == "1" && ( survey.questions.length >= played_question ) ) {
				percent = Math.ceil( ( played_question / survey.questions.length ) * 100 );
				survey_progress_bar = '<div class="survey-progress-bar"><div class="survey-progress-ln"><span class="progress_counter">0%</span></div><div class="survey-progress-ln2"><span class="progress"></span></div></div>';
				if ( played_question > 1 ) {
					percent_last = Math.ceil( ( parseInt( played_question - 1 ) / parseInt( survey.questions.length ) ) * 100 );
					survey_progress_bar = '<div class="survey-progress-bar"><div class="survey-progress-ln"><span class="progress_counter">' + percent_last + '%</span></div><div class="survey-progress-ln2"><span class="progress" style="width: ' + percent_last + '%"></span></div></div>';
				}
				if ( played_question == 1 ) {
					percent_last = 0;
				}
			}
			else {
				survey_progress_bar = "";
			}
			modal_survey.find( ".progress" ).css( "width", percent_last + "%" );
			if ( played > 0 && ( survey.style == 'click' ) ) {
				survey.expired = "true";
				if ( survey.message == undefined ) {
					survey.message = 'You already filled out this survey!';
				}
			}
			if ( ( survey.expired == 'true' ) && ( survey.style != 'click' ) ) {
				return true;
			}
			if ( typeof survey.questions[ played_question ] != 'undefined' ) {
				survey.questions[ played_question ][ 'hasopen' ] = false;
			}
			if ( ( ( survey.questions.length > 0 ) || ( survey.questions.length < played_question + 1 ) ) || ( survey.expired == 'true' ) ) {
				if ( survey.style != 'flat' ) {
					modal_survey.css( "top", "" );
					modal_survey.css( "bottom", "" );
					modal_survey.css( survey.align, "0px" );
					if ( parseInt( survey_options[ 14 ] ) == 1 ) {
						if ( survey_options[ 0 ] == "top" ) {
							remove_image = "<img id='close_survey' class='cl_top_survey closeimg_" + survey_options[ 145 ] + "' src='" + survey.plugin_url + "/templates/assets/img/" + survey_options[ 141 ] + ".png' />";
						}
						else {
							remove_image = "<img id='close_survey' class='cl_survey closeimg_" + survey_options[ 145 ] + "' src='"+survey.plugin_url + "/templates/assets/img/" + survey_options[ 141 ] + ".png' />";
						}
					}
				}
				if ( ( survey.questions.length - 1 ) >= played_question && ( survey.expired != 'true' ) ) {
					if ( isUrlValid( survey_qoptions[ played_question ][ 2 ] ) ) {
						qimg = "<img class='survey_question_image' src='" + survey_qoptions[ played_question ][ 2 ] + "'>";
					}
					else {
						qimg = "";
					}
					chtitle = survey.questions[ played_question ][ 0 ].match(/\[(.*)\]/);
					if ( chtitle != null ) {
						question_name[ parseInt( played_question ) ] = chtitle[ 1 ];
					}
					else {
						question_name[ parseInt( played_question ) ] = survey.questions[ played_question ][ 0 ];
					}
					survey_content += remove_image + '<div class="survey_table ' + tableclass + '"><div class="survey_element survey_question">' +  qimg + '<span>' + survey.questions[ played_question ][ 0 ].replace(/\[(.*?)\]/g, "") + '</span></div>';
					var answers_number = -2, separator, different = 0, counter = 0, qstepn, acat;
					for ( i in survey.questions[ played_question ] ) {
						if ( survey.questions[ played_question ].hasOwnProperty( i ) ) {
							answers_number++;
						}
					}
					if ( answers_number < 5 || answers_number % 4 == 0 ) {
						separator = 4;
					}
					if ( answers_number % 3 == 0 ) {
						separator = 3;
					}
					if ( answers_number % 4 > 0 ) {
						separator = 4;
						different = answers_number % 4;
					}
					if ( answers_number % 3 > 0 ) {
						separator = 3;
						different = answers_number % 3;
					}
					if ( survey.grid_items > 0 ) {
						separator = survey.grid_items;
						different = answers_number % survey.grid_items;
					}
					if ( detectmob() == true || ( modal_survey.parent().width() < 500 && survey.style == 'flat' ) ) {
						separator = 1;
						different = 0;
					}
					if ( survey_options[ 22 ] == 1 ) {
						separator = 1;
					}
					for ( key in survey.questions[ played_question ] ) {
						if ( key != 0 && jQuery.isNumeric( key ) ) {
							if ( ( ( key-1 ) == ( answers_number - different ) ) && ( survey_qoptions[ played_question ][ 3 ] != 1 ) ) {
							  survey_content += '</div></div><div class="survey_table" style="margin-top:-10px;">';
							}
							if ( ( counter == separator || counter == 0 ) && ( survey_qoptions[ played_question ][ 3 ] != 1 ) ) {
								survey_content += '<div class="survey_element survey_row">';
							}
							counter++;
							qstep = survey.questions[ played_question ][ key ].match(/\[(.*)\]/);
							if ( qstep != null ) {
								if ( qstep[ 1 ] >= 0 ) {
									qstepn = qstep[ 1 ];
								}
								else {
									acat = qstep[ 1 ];
									qstepn = '';
								}
							}
							else {
								qstepn = '';
							}
							if ( survey_qoptions[ played_question ][ 3 ] != 1 ) {
								oindex = parseInt( played_question + 1 ) + "_" + key;
								if ( survey_aoptions[ oindex ][ 0 ] == "open" ) {
									survey_content += '<div class="survey_element survey_answers ' + survey_options[ 134 ] + ' survey_open_answers" data-qstep="' + qstepn + '" qid="' + played_question + '" id="survey_answer' + ( parseInt( key ) ) + '"><input list="ms_answers_' + survey.survey_id + '_' + survey_aoptions[ oindex ][ 1 ] + '" type="text" maxlength="100" name="open_answer" class="open_text_answer" value="" placeholder="' + survey.questions[ played_question ][ key ].replace( '[' + qstepn + ']', '' ).replace( '[' + acat + ']', '' ) + '"></div>';
									survey.questions[ played_question ][ 'hasopen' ] = true;
								}
								else {
									if ( isUrlValid( survey_aoptions[ oindex ][ 3 ] ) ) {
										if ( survey_aoptions[ oindex ][ 6 ] != "" ) {
											imgwidth = survey_aoptions[ oindex ][ 6 ];
										}
										else {
											imgwidth = "";
										}
										if ( survey_aoptions[ oindex ][ 7 ] != "" ) {
											imgheight = survey_aoptions[ oindex ][ 7 ];
										}
										else {
											imgheight = "";
										}
										imgstyle = "";
										if ( imgwidth != "" && imgheight == "" ) {
											imgstyle = "style='width: " + imgwidth + ";max-width: " + imgwidth + "'";
										}
										if ( imgwidth == "" && imgheight != "" ) {
											imgstyle = "style='height: " + imgheight + ";max-height: " + imgheight + "'";
										}
										if ( imgwidth != "" && imgheight != "" ) {
											imgstyle = "style='width: " + imgwidth + ";max-width: " + imgwidth + ";height: " + imgheight + ";max-height: " + imgheight + "'";
										}
										answerimg = "<img class='survey_answer_image' " + imgstyle + " src='" + survey_aoptions[ oindex ][ 3 ] + "'>";
									}
									else {
										answerimg = "";
									}
									survey_content += '<div class="survey_element survey_answers ' + survey_options[ 134 ] + '" qid="' + played_question + '" data-qstep="' + qstepn + '" id="survey_answer' + ( parseInt( key ) ) + '">' + answerimg + survey.questions[ played_question ][ key ].replace( '[' + qstepn + ']', '' ).replace( '[' + acat + ']', '' ) + '</div>';
								}
							}
							else {
								if ( survey.questions[ played_question ][ key ].replace( '[' + qstepn + ']', '' ).replace( '[' + acat + ']', '' ) == "" ) {
									tooltip = '';
								}
								else {
									tooltip = ' data-tooltip="' + survey.questions[ played_question ][ key ].replace( '[' + qstepn + ']', '' ).replace( '[' + acat + ']', '' ) + '"';
								}
								answerimg = '<span ' + tooltip + '><div class="survey_rating survey_answers" qid="' + played_question + '" data-qstep="' + qstepn + '" id="survey_answer' + ( parseInt( key ) ) + '"></div></span>'
								if ( counter == 1 ) {
									survey_content += '</div></div><div class="survey_table" style="margin-top:-10px;"><div class="survey_element survey_row"><div class="survey_element survey_answers ms_rating_question" qid="' + played_question + '" data-qstep="' + qstepn + '">';												
								}
								survey_content += answerimg;			
								if ( counter == answers_number ) {
									survey_content += '</div></div>';												
								}
							}
							if ( ( counter == separator ) && ( survey_qoptions[ played_question ][ 3 ] != 1 ) ) {
								survey_content += '</div>';
								counter = 0;
							}
						}
					};
					//choices
					if ( survey_qoptions[ played_question ][ 0 ] == 'undefined' || survey_qoptions[ played_question ][ 0 ] == '' || survey_qoptions[ played_question ][ 0 ] == '0' ) {
						survey_qoptions[ played_question ][ 0 ] = 1;
					}
					//minchoices
					if ( survey_qoptions[ played_question ][ 1 ] == 'undefined' || survey_qoptions[ played_question ][ 1 ] == '' || survey_qoptions[ played_question ][ 1 ] == '0' ) {
						survey_qoptions[ played_question ][ 1 ] = 1;
					}
					if ( survey_qoptions[ played_question ][ 3 ] == 1 ) {
						survey_qoptions[ played_question ][ 0 ] = 1;						
						survey_qoptions[ played_question ][ 1 ] = 1;
					}
					if ( survey_qoptions[ played_question ][ 0 ] > 1 || survey.questions[ played_question ][ 'hasopen' ] == true ) {
						next_button = '<div class="survey_element survey_answers survey_answer_choice" qid="' + played_question + '" data-qstep="' + qstepn + '" id="surveychoice' + survey.id + '_' + played_question + '"> <img src="' + survey.plugin_url + '/templates/assets/img/next-passive.png"> </div>';
					}
					if ( different == 1 ) {
						survey_content += '</div>';
					}
				}
				else {
					var display = " ";
					if ( survey.survey_conds != "" && ( endstep == played_question ) ) {
						random = Math.floor(Math.random()*(1000000-1000+1)+1000);
						jQuery.map( survey.survey_conds, function( val, i ) {
							var target = "";
							var qscexp = "";
							var newval = JSON.parse( val );
							if ( newval[ 0 ] == "score" ) target = surveyscore;
							if ( newval[ 0 ] == "correct" ) target = surveycorrect;
							if ( newval[ 0 ].indexOf( "questionscore_" ) >= 0 ) {
								qscexp = newval[ 0 ].split("_");
								target = question_score[ qscexp[ 1 ] ];
							}
							if ( newval[ 0 ].indexOf( "questioncatscore_" ) >= 0 ) {
								qscexp = newval[ 0 ].split("_");
								target = surveycatscore[ qscexp[ 1 ] ];
								if ( newval[ 2 ] == "highest" ) {
									newval[ 2 ] = surveycatscore[ array_max( surveycatscore ) ];
								}
								if ( newval[ 2 ] == "lowest" ) {
									newval[ 2 ] = surveycatscore[ array_min( surveycatscore ) ];
								}
							}
							if ( newval[ 1 ] == "higher" && target > parseInt( newval[ 2 ] ) ) {
								if ( newval[ 3 ] == "display" ) {
									display += "<p>" + newval[ 4 ].replace(/[|]/g, "'") + "</p>";
								}
								if ( newval[ 3 ] == "soctitle" ) {
									soctitle = newval[ 4 ];
								}
								if ( newval[ 3 ] == "socdesc" ) {
									socdesc = newval[ 4 ];
								}
								if ( newval[ 3 ] == "socimg" ) {
									socimg = newval[ 4 ];
								}
								if ( newval[ 3 ] == "redirect" ) {
									setTimeout( function() {
										window.location.href = newval[ 4 ];
									}, parseInt( survey_options[ 23 ] ) );
								}
								if ( newval[ 3 ].indexOf( "questionscore" ) >= 0 ) {
									setTimeout( function() {
										window.location.href = newval[ 4 ];
									}, parseInt( survey_options[ 23 ] ) );
								}
								if ( newval[ 3 ] == "displayirchart" || newval[ 3 ] == "displayischart" || newval[ 3 ] == "displayicchart" ) {
									display += "<div id='survey-results-" + random + "' class='displaychart survey-results'><div class='modal-survey-chart0'><canvas style='width: 100%; height: 100%;'></canvas></div></div>";
									if ( ( newval[ 4 ] == "" ) && ( newval[ 3 ] == "displayischart" || newval[ 3 ] == "displayirchart" || newval[ 3 ] == "displayicchart" ) ) {
										charttype = "radarchart";
									}
									if ( ( newval[ 4 ] != "" ) && ( newval[ 3 ] == "displayischart" || newval[ 3 ] == "displayirchart" || newval[ 3 ] == "displayicchart" ) ) {
										charttype = newval[ 4 ];
									}
									chartmode = newval[ 3 ];
								}
							}
							if ( newval[ 2 ].toString().indexOf( "-" ) >= 0 ) {
								between = newval[ 2 ].toString().split( "-" );
							}
							if ( between == undefined ) {
								between[ 0 ] = 0;
								between[ 1 ] = 0;
							}
							if ( newval[ 1 ] == "equal" && ( target == parseInt( newval[ 2 ] ) || ( target >= parseInt( between[ 0 ] ) && target <= parseInt( between[ 1 ] ) ) ) ) {
								if ( newval[ 3 ] == "display" ) {
									display += "<p>" + newval[ 4 ].replace(/[|]/g, "'") + "</p>";
								}
								if ( newval[ 3 ] == "soctitle" ) {
									soctitle = newval[ 4 ];
								}
								if ( newval[ 3 ] == "socdesc" ) {
									socdesc = newval[ 4 ];
								}
								if ( newval[ 3 ] == "socimg" ) {
									socimg = newval[ 4 ];
								}
								if ( newval[ 3 ] == "redirect" ) {
									setTimeout( function() {
										window.location.href = newval[ 4 ];
									}, parseInt( survey_options[ 23 ] ) );
								}
								if ( newval[ 3 ] == "displayirchart" || newval[ 3 ] == "displayischart" || newval[ 3 ] == "displayicchart" ) {
									display += "<div id='survey-results-" + random + "' class='displaychart survey-results'><div class='modal-survey-chart0'><canvas style='width: 100%; height: 100%;'></canvas></div></div>";
									if ( ( newval[ 4 ] == "" ) && ( newval[ 3 ] == "displayischart" || newval[ 3 ] == "displayirchart" || newval[ 3 ] == "displayicchart" ) ) {
										charttype = "radarchart";
									}
									if ( ( newval[ 4 ] != "" ) && ( newval[ 3 ] == "displayischart" || newval[ 3 ] == "displayirchart" || newval[ 3 ] == "displayicchart" ) ) {
										charttype = newval[ 4 ];
									}
									chartmode = newval[ 3 ];
								}
							}
							if ( newval[ 1 ] == "lower" && target < parseInt( newval[ 2 ] ) ) {
								if ( newval[ 3 ] == "display" ) {
									display += "<p>" + newval[ 4 ].replace(/[|]/g, "'") + "</p>";
								}
								if ( newval[ 3 ] == "soctitle" ) {
									soctitle = newval[ 4 ];
								}
								if ( newval[ 3 ] == "socdesc" ) {
									socdesc = newval[ 4 ];
								}
								if ( newval[ 3 ] == "socimg" ) {
									socimg = newval[ 4 ];
								}
								if ( newval[ 3 ] == "redirect" ) {
									setTimeout( function() {
										window.location.href = newval[ 4 ];
									}, parseInt( survey_options[ 23 ] ) );
								}
								if ( newval[ 3 ] == "displayirchart" || newval[ 3 ] == "displayischart" || newval[ 3 ] == "displayicchart" ) {
									display += "<div id='survey-results-" + random + "' class='displaychart survey-results'><div class='modal-survey-chart0'><canvas style='width: 100%; height: 100%;'></canvas></div></div>";
									if ( ( newval[ 4 ] == "" ) && ( newval[ 3 ] == "displayischart" || newval[ 3 ] == "displayirchart" || newval[ 3 ] == "displayicchart" ) ) {
										charttype = "radarchart";
									}
									if ( ( newval[ 4 ] != "" ) && ( newval[ 3 ] == "displayischart" || newval[ 3 ] == "displayirchart" || newval[ 3 ] == "displayicchart" ) ) {
										charttype = newval[ 4 ];
									}
									chartmode = newval[ 3 ];
								}
							}
						});
					}
					if ( survey.social[ 3 ] == "endcontent" ) {
						display += socialbuttons;
					}
					if ( ( ( survey_options[ 125 ] == 1 && survey.form != 'false' ) || survey.form == 'true' ) && ( survey_options[ 126 ] == 1 || survey_options[ 127 ] == 1 ) && ( pformstep == played_question ) ) {
						survey_participant_form = '<div class="ms-participant-form"><p>' + survey.languages.pform_description + '</p>';
						if ( survey_options[ 126 ] == 1 ) {
							survey_participant_form += '<input type="text" class="ms-form-name" placeholder="' + survey.languages.name_placeholder + '" value="' +  survey.user.name + '">';
						}
						if ( survey_options[ 127 ] == 1 ) {
							survey_participant_form += '<input type="text" class="ms-form-email" placeholder="' + survey.languages.email_placeholder + '" value="' +  survey.user.email + '">';
						}
						survey_participant_form += '<div class="send-participant-form-container"><a class="send-participant-form button button-secondary button-default">' + survey.languages.send_button + '</a></div></div>';
					}
					if ( endstep == played_question ) {
						if ( survey_options[ 12 ].indexOf( "[noclose]" ) > 0 ) {
							survey_options[ 12 ]  = survey_options[ 12 ].replace( "[noclose]", "" );
							noclose = "true";
						}
						if ( display.indexOf( "[noclose]" ) > 0 ) {
							display  = display.replace( "[noclose]", "" );
							noclose = "true";
						}
						if ( survey_options[ 12 ] != "" ) {
							survey_content += '<div class="survey_table endcontent"><div class="survey_element survey_question"><span><p>' + survey_options[ 12 ].replace( '[score]', surveyscore ).replace( '[correct]', surveycorrect ).replace(/[|]/gi, "'" ) + '</p>' + display + '</span></div></div>';
						}
					}
					if ( ( pformstep == played_question ) && ( survey_participant_form != "" ) ) {
						survey_content += '<div class="survey_table ' + tableclass + '"><div class="survey_element survey_question">' + survey_participant_form + '</div></div>';
					}
					if ( survey_content == "" ) {
						survey_content += '<div class="survey_element survey_question" style="display:none;"></div>'
					}
				}
				if ( jQuery( '#' + survey.survey_id + ' #question_' + played_question + ' .answer' ).length < 3 ) {
					survey_content += '</div></div>';
				}
				if ( survey.display == "all" && survey.social[ 3 ] == "bottom" ) {
					survey.social[ 3 ] = "end";
				}
				if ( survey.social[ 3 ] == "bottom" || ( survey.social[ 3 ] == "end" && played_question == survey.questions.length ) ) {
					survey_content += socialbuttons;
				}
				if ( survey.display == "all" && survey_participant_form == "" && endstep == played_question + 1 ) {
					survey_content += '<div class="survey_table ' + tableclass + '"><div><div class="send-participant-form-container"><a class="send-participant-form button button-secondary button-default">' + survey.languages.send_button + '</a></div></div></div></div>';
				}
				modal_survey.html( survey_content + survey_progress_bar );
				if ( survey.display == "all" && survey_participant_form == "" && endstep == played_question + 1 ) {
					jQuery( "body" ).on( "click", "#survey-" + survey.survey_id + "-" + unique_key + " .send-participant-form", function() {
						if ( send_bulk_answers() != true ) {
							rmdni = false;
							return;
						}
					})
				}
				if ( endstep == played_question ) {
					if ( soctitle == "" && socdesc == "" && socimg == "" ) {
						survey.social[ 6 ] = survey.social[ 6 ].replace( '{score}', surveyscore ).replace( '{correct}', surveycorrect ).replace( '[score]', surveyscore ).replace( '[correct]', surveycorrect );
						survey.social[ 7 ] = survey.social[ 7 ].replace( '{score}', surveyscore ).replace( '{correct}', surveycorrect ).replace( '[score]', surveyscore ).replace( '[correct]', surveycorrect );
						jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .share-twitter" ).attr( "href", "http://twitter.com/share?url=" + survey.social[ 4 ] + "&amp;text=" + survey.social[ 6 ] );
						jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .share-pinterest" ).attr( "href", "http://pinterest.com/pin/create/button/?url=" + survey.social[ 4 ] + "&amp;media=" + survey.social[ 5 ] + "&amp;description=" + survey.social[ 6 ] );
						jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .share-linkedin" ).attr( "href", "https://www.linkedin.com/shareArticle?mini=true&url=" + survey.social[ 4 ] + "&title=" + survey.social[ 6 ] + "&summary=" + survey.social[ 7 ] );
						if ( survey.social[ 8 ] != "" ) {
							jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .share-facebook" ).attr( "href", "https://www.facebook.com/dialog/feed?app_id=" + survey.social[ 8 ] + "&display=popup&name=" + survey.social[ 6 ] + "&caption=" + survey.social[ 7 ] + "&picture=" + survey.social[ 5 ] + "&link=" + survey.social[ 4 ] + "&redirect_uri=" + survey.social[ 4 ] );
						}
					}
					else {
						setsocialmetas();
					}
					var data = {
						action: 'ajax_survey_answer',
						sspcmd: 'displaychart',
						sid: survey.survey_id,
						};
						jQuery.post( survey.admin_url, data, function( response ) {
							if ( response != "" ) {
								var resp = response.split( "|endcontent-params|" );
								modal_survey.find( ".endcontent .survey_element" ).append( resp[ 0 ] );
								var cfg = JSON.parse( resp[ 1 ] );
								jQuery( "#survey-results-" + survey.survey_id + "-endcontent" ).pmsresults({ 
									"style": cfg.style, "datas": cfg.datas 
								});
							}
						})							
				}
				if ( survey.style == "flat" ) {
					var contwidth = modal_survey.parent().width();
				}
				else {
					var contwidth = modal_survey.width();
				}
				jQuery( "#survey-results-" + random + ", #survey-results-" + random + " div"  ).css({ "width": parseInt( contwidth * 0.75 )+"px", height: parseInt( jQuery( window ).height() * 0.5 ) + "px" })

				if ( next_button != "" && survey.display != 'all' ) {
					jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .survey_question" ).append( next_button );
				}				
				//make the style
				set_survey_style();
				//make animation
				make_animation();
				if ( survey_options[ 20 ] == "1" && played_question > 0 ) {
					setTimeout( function() {
						jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .progress" ).css( "width", percent + "%" );
						countTo( modal_survey.find( ".progress_counter" ), parseInt( percent_last ), parseInt( percent ), 500, 50, "%", 0, null, null );
					}, parseInt( survey_options[ 11 ] ) );
				}
			}
			if ( ( ( survey.questions.length - 1 ) < played_question ) || ( survey.expired == 'true' ) || ( played_question == endstep && survey.display == "all" ) ) {
				played++;
				if ( survey.style == 'click' ) {
					if ( survey.expired == 'true' ) {
						jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .survey_question" ).html( '<span>' + survey.message.replace( '[score]', surveyscore ).replace( '[correct]', surveycorrect ).replace( /[|]/gi, "'" ) + '</span>' );
					}
				}
				if ( jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .survey_question" ).length > 0 ) {
					if ( ( jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .survey_question" ).html().indexOf( "<a " ) >= 0 || noclose == "true" || survey_options[ 23 ] == 0 ) || ( survey.display == "all" ) ) {
						jQuery( "body" ).on( "click", "#survey-" + survey.survey_id + "-" + unique_key + " .survey_question a", function() {
							if ( jQuery( this ).hasClass( "send-participant-form" ) ) {
								if ( rmdni == false ) {
									if ( survey.display == "all" ) {
										if ( send_bulk_answers() != true ) {
											rmdni = false;
											return;
										}
									}
									sendbutton = jQuery( this ).parent();
									rmdni = true;
									senderror = false;
									if ( survey_options[ 125 ] == 1 && ( survey_options[ 126 ] == 1 || survey_options[ 127 ] == 1 ) ) {
										if ( survey_options[ 126 ] == 1 ) {
											if ( jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .ms-form-name" ).val().length < 3 ) {
												inputtemp = jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .ms-form-name" ).val();
												jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .ms-form-name" ).css( "color", "#FC0303" );
												jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .ms-form-name" ).val( "Name too short" );
												setTimeout( function() {
													jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .ms-form-name" ).css( "color", "" );
													jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .ms-form-name" ).val( inputtemp );
													rmdni = false;
												}, 2000);
												senderror = true;
											}
										}
										if ( survey_options[ 127 ] == 1 ) {
											if ( ! isValidEmailAddress( jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .ms-form-email" ).val() ) && survey_options[ 146 ] == 1 ) {
												inputtemp = jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .ms-form-email" ).val();
												jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .ms-form-email" ).css( "color", "#FC0303" );
												jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .ms-form-email" ).val( "Invalid Email Address" );
												setTimeout( function() {
													jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .ms-form-email" ).css( "color", "" );
													jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .ms-form-email" ).val( inputtemp );
													rmdni = false;
												}, 2000 );
												senderror = true;
											}
										}
									}
									if ( senderror == false ) {
										var data = {
												action: 'ajax_survey_answer',
												sspcmd: 'form',
												endcontent: true,
												name: jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .ms-form-name" ).val(),
												email: jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .ms-form-email" ).val(),
												sid: survey.survey_id
											};
										jQuery( sendbutton ).html('<img width="20" src="' + survey.plugin_url + '/templates/assets/img/' + survey_options[ 133 ] + '.gif">');
										jQuery.post( survey.admin_url, data, function( response ) {
											if ( response.toLowerCase().indexOf( "success" ) >= 0 ) {
												jQuery( sendbutton ).html( survey.languages.success );
												played_question++;
												continue_survey();
												/*setTimeout( function() {
													if ( ! jQuery( this ).hasClass( "ms-social-share" ) ) {
														close_survey( jQuery( this ) );
													}
												}, 1000 );*/
											}
											rmdni = false;
										})
									}				
								}
								else {
									if ( ! jQuery( this ).hasClass( "ms-social-share" ) ) {
										close_survey();
									}
								}
							}
						})
					}
					else {
						setTimeout( function() {
							if ( survey.style == 'flat' ) {
								modal_survey.slideUp( parseInt( survey_options[ 11 ] ), survey_options[ 1 ]);
							}
							else {
								if ( survey_options[ 0 ] == "bottom" ) {
									modal_survey.animate({ bottom: "-" + parseInt( modal_survey.height() + 100 ) + "px" }, parseInt( survey_options[ 11 ] ), survey_options[ 1 ], function() {
										modal_survey.css( "bottom", "-" + parseInt( modal_survey.height() + 100 ) + "px");
										modal_survey.css( "display", "table" );
										});
								}
								if ( survey_options[ 0 ] == "center" ) {
									if ( survey.align == "left" ) {
										modal_survey.animate({ left: "-5000px" }, parseInt( survey_options[ 11 ] ), survey_options[ 1 ], function(){
											modal_survey.css( "bottom", "-" + parseInt( modal_survey.height() + 100) + "px");
											modal_survey.css( "display", "table" );
										});
									}
									else {
										modal_survey.animate({ right: "-5000px" }, parseInt( survey_options[ 11 ] ), survey_options[ 1 ], function() {
											modal_survey.css( "bottom", "-" + parseInt( modal_survey.height() + 100 ) + "px" );
											modal_survey.css( "display", "table" );
										})							
									}
								}
								if ( survey_options[ 0 ] == "top" ) {
									modal_survey.animate({ top: "-" + parseInt( modal_survey.height() + 100 ) + "px" }, parseInt( survey_options[ 11 ] ), survey_options[ 1 ], function(){
										modal_survey.css( "bottom", "-" + parseInt( modal_survey.height() + 100) + "px");
										modal_survey.css( "display", "table" );
									})
								}
								if ( ( jQuery( "#bglock" ).length > 0 ) && ( jQuery( "#bglock" ).css( "display" ) == "block" ) && ( parseInt( survey_options[ 13 ] ) ) == 1 ) {
									jQuery( "#bglock" ).fadeOut( 1000, function() {
										jQuery( "#bglock" ).remove();
									});
								}
								played_question = -1;
							}
							if ( survey_options[ 19 ] != "" ) {
								if ( isUrlValid( survey_options[ 19 ] ) ) {
									window.location.href = survey_options[ 19 ];
								}
							}
						}, parseInt( survey_options[ 23 ] ) );
					}
				}
			}
			if ( survey.style == 'flat' && survey.display == 'all' ) {
				while ( endstep > parseInt( played_question + 1 ) ) {
					alldisplay_survey_content = survey_content;
					played_question++;
					play_survey();
				}
			}
		}
		
		function send_bulk_answers() {
			var bulkanswers = {}, ba = [], bulkopen = {}, bo = [], count_answers_num = 0;
			var thiselem = jQuery( ".send-participant-form-container .button" );
			for ( i = 0; i < survey.questions.length; ++i ) {
				ba = [];bo = [];
				modal_survey.find( ".survey_answers" ).each( function( index ) {
					if ( ( jQuery( this ).hasClass( survey_options[ 134 ] + "selected" ) >= 1 || jQuery( this ).hasClass( "selected" ) ) && ( jQuery( this ).attr( "qid" ) == i ) && ( ! jQuery( this ).hasClass( "ms_rating_question" ) ) ) {
						ba.push( jQuery( this ).attr( "id" ).replace( "survey_answer", "" ) );
						if ( jQuery( this ).hasClass( "survey_open_answers" ) ) {
							bo.push( jQuery( this ).children( "input" ).val() );
						}
						else {
							bo.push( "" );
						}
					}
				})
				if ( ba.length > 0 ) {
					bulkanswers[ i + 1 ] = ba;
					bulkopen[ i + 1 ] = bo;
					count_answers_num++;
				}
				if ( survey_qoptions[ i ][ 3 ] == 1 ) {
					thiselem.parent().append( '<div id="survey_preloader"><img width="20" src="' + survey.plugin_url + '/templates/assets/img/' + survey_options[ 133 ] + '.gif"></div>' );
				}
				else {
					thiselem.append( '<div id="survey_preloader"><img width="20" src="' + survey.plugin_url + '/templates/assets/img/' + survey_options[ 133 ] + '.gif"></div>' );
				}
				var thissurvey = [], qa = {}, thisscore = 0, thiscorrect = 0;
				var regExp = /\[(.*?)\]/;
				var matches = regExp.exec( survey.questions[ i ][ 0 ] );
				if ( matches != null ) {
					if ( surveycatscore[ matches[ 1 ] ] == undefined ) {
						surveycatscore[ matches[ 1 ] ] = 0;
					}
				}
				jQuery.map( survey.questions[ i ], function( val, i ) {
					if ( i > 0 ) {
						var matches2 = regExp.exec( val );
						if ( matches2 != null ) {
							if ( surveycatscore[ matches2[ 1 ] ] == undefined && ! jQuery.isNumeric( matches2[ 1 ] ) ) {
								surveycatscore[ matches2[ 1 ] ] = 0;
							}
						}
					}
				})
				jQuery.map( ba, function( val, f ) {
					if ( parseInt( survey_aoptions[ parseInt( i + 1 ) + "_" + val ][ 4 ] ) >= 0 ) {
						thisscore = parseInt( survey_aoptions[ parseInt( i + 1 ) + "_" + val ][ 4 ] );
					}
					else {
						thisscore = 0;
					}
					if ( parseInt( survey_aoptions[ parseInt( i + 1 ) + "_" + val ][ 5 ] ) >= 0 ) {
						thiscorrect = parseInt( survey_aoptions[ parseInt( i + 1 ) + "_" + val ][ 5 ] );
					}
					else {
						thiscorrect = 0;
					}
					if ( matches != null ) {
						if ( matches[ 1 ] != undefined ) {
							surveycatscore[ matches[ 1 ] ] = parseInt( surveycatscore[ matches[ 1 ] ] ) + thisscore;
						}
					}
					var matches3 = regExp.exec( survey.questions[ i ][ parseInt( val ) ] );
					if ( matches3 != null ) {
							if ( matches3 != null ) {
								if ( matches3[ 1 ] != undefined && ! jQuery.isNumeric( matches3[ 1 ] ) ) {
									surveycatscore[ matches3[ 1 ] ] = parseInt( surveycatscore[ matches3[ 1 ] ] ) + thisscore;
								}
							}
					}
					surveyscore = parseInt( surveyscore ) + thisscore;
					surveycorrect = parseInt( surveycorrect ) + thiscorrect;
					question_score[ parseInt( i + 1 ) ] = thisscore;
					question_correct[ parseInt( i + 1 ) ] = thiscorrect;
					question_choice[ parseInt( i + 1 ) ] = ba;
				});
				if ( survey.preview == undefined ) {
					survey.preview = "false";
				}
			}
			qa[ 'sid' ] = survey.survey_id;
			qa[ 'bulkans' ] = bulkanswers;
			qa[ 'auto_id' ] = survey.auto_id;
			qa[ 'postid' ] = survey.postid;
			qa[ 'bulkopen' ] = bulkopen;
			thissurvey.push( qa );
			rmdni = true;
			if ( count_answers_num < survey.questions.length ) {
				jQuery( "html, body" ).animate({
					scrollTop: modal_survey.offset().top - 200
				}, 500 );
				return false;
			}
			else {
				var data = {
					action: 'ajax_survey_answer',
					sspcmd: 'bulksave',
					endcontent: true,
					options: JSON.stringify( thissurvey ),
					preview: survey.preview
					};
					jQuery.post( survey.admin_url, data, function( response ) {
						if ( response.toLowerCase().indexOf( "success" ) >= 0 ) {	
							jQuery( "#survey_preloader" ).remove();
							//make animation
							played_question++;
							continue_survey();
						}
						rmdni = false;
						return true;
					}).fail(function() {
							jQuery( "#survey_preloader" ).remove();						
							rmdni = false;
							sanswers = [];
							return false;
					  })
			}
		}
		
		function setsocialmetas() {
			if ( soctitle != "" ) {
				soctitle = soctitle.replace( '{score}', surveyscore ).replace( '{correct}', surveycorrect ).replace( '[score]', surveyscore ).replace( '[correct]', surveycorrect );
			}
			else {
				soctitle = survey.social[ 6 ].replace( '{score}', surveyscore ).replace( '{correct}', surveycorrect ).replace( '[score]', surveyscore ).replace( '[correct]', surveycorrect );				
			}
			if ( socdesc != "" ) {
				socdesc = socdesc.replace( '{score}', surveyscore ).replace( '{correct}', surveycorrect ).replace( '[score]', surveyscore ).replace( '[correct]', surveycorrect );
			}
			else {
				socdesc = survey.social[ 7 ].replace( '{score}', surveyscore ).replace( '{correct}', surveycorrect ).replace( '[score]', surveyscore ).replace( '[correct]', surveycorrect );
			}
			if ( socimg != "" ) {
				socimg = socimg.replace( '{score}', surveyscore ).replace( '{correct}', surveycorrect ).replace( '[score]', surveyscore ).replace( '[correct]', surveycorrect );
			}
			else {
				socimg = survey.social[ 5 ].replace( '{score}', surveyscore ).replace( '{correct}', surveycorrect ).replace( '[score]', surveyscore ).replace( '[correct]', surveycorrect );
			}
			jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .share-twitter" ).attr( "href", "http://twitter.com/share?url=" + survey.social[ 4 ] + "&amp;text=" + soctitle );
			jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .share-pinterest" ).attr( "href", "http://pinterest.com/pin/create/button/?url=" + survey.social[ 4 ] + "&amp;media=" + survey.social[ 5 ] + "&amp;description=" + soctitle );
			jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .share-linkedin" ).attr( "href", "https://www.linkedin.com/shareArticle?mini=true&url=" + survey.social[ 4 ] + "&title=" + soctitle + "&summary=" + socdesc );
			if ( survey.social[ 8 ] != "" ) {
				jQuery( "#survey-" + survey.survey_id + "-" + unique_key + " .share-facebook" ).attr( "href", "https://www.facebook.com/dialog/feed?app_id=" + survey.social[ 8 ] + "&display=popup&name=" + soctitle + "&caption=" + socdesc + "&picture=" + socimg + "&link=" + survey.social[ 4 ] + "&redirect_uri=" + survey.social[ 4 ] );
			}			
		}
		
		/*		Email Validation Function		*/
		function isValidEmailAddress( emailAddress ) {
			var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
			return pattern.test( emailAddress );
		};
		
		function close_survey() {
				if (survey_options[0]=="bottom") {
					modal_survey.animate({ bottom: "-" + parseInt( modal_survey.height() + 100 ) + "px" }, parseInt( survey_options[ 11 ] ), survey_options[ 1 ], function() { 
						modal_survey.css( "bottom", "-" + parseInt( modal_survey.height() + 100 ) + "px" );
						modal_survey.css( "display", "table" );
					})
				}
				if ( survey_options[ 0 ] == "center" ) {
					if ( survey.align == "left" ) {
						modal_survey.animate({ left: "-5000px" }, parseInt( survey_options[ 11 ] ), survey_options[ 1 ], function() {
							modal_survey.css( "bottom", "-" + parseInt( modal_survey.height() + 100 ) + "px" );
							modal_survey.css( "display", "table" );
						})
					}
					else {
						modal_survey.animate({ left: "-5000px" }, parseInt( survey_options[ 11 ] ), survey_options[ 1 ], function() {
							modal_survey.css( "bottom", "-" + parseInt( modal_survey.height() + 100 ) + "px" );
							modal_survey.css( "display", "table" );
						})
					}
				}
				if ( survey_options[ 0 ] == "top" ) {
					modal_survey.animate({ top: "-" + parseInt( modal_survey.height() + 100 ) + "px" }, parseInt( survey_options[ 11 ] ), survey_options[ 1 ], function() {
						modal_survey.css( "bottom", "-" + parseInt( modal_survey.height() + 100 ) + "px" );
						modal_survey.css( "display", "table" );
					})
				}
				if ( jQuery( "#bglock" ).length > 0 && jQuery( "#bglock" ).css( "display", "block" ) && parseInt( survey_options[ 13 ] ) == 1) {
					jQuery( "#bglock" ).fadeOut( 1000, function() { 
						jQuery( "#bglock" ).remove();
					});
				}
				played_question = -1;
		}

		function display_chart() {
			if ( ( charttype != undefined ) && ( played_question == endstep ) ) {
				jQuery.map( question_name, function( val, i ) {
				var exist = 0;
					if ( chartmode == "displayischart" ) {
						chartelems[ 'answer' ] = val;
						chartelems[ 'count' ] = question_score[ i + 1 ];
					}
					if ( chartmode == "displayirchart" ) {
						chartelems[ 'answer' ] = val;
						chartelems[ 'count' ] = question_choice[ i + 1 ][ 0 ];					
					}
					if ( chartmode == "displayicchart" ) {
						chartelems[ 'answer' ] = val;
						chartelems[ 'count' ] = question_correct[ i + 1 ];					
					}
					if ( chartelems[ 'count' ] == undefined ) {
						chartelems[ 'count' ] = 0;
					}
					if ( val != undefined ) {
						if ( jQuery.isEmptyObject( chartparams ) ) {
									chartparams.push( {"answer": val, "count": chartelems[ 'count' ]} );							
						}
						else {
							jQuery.map( chartparams, function( cp, k ) {
								if ( cp[ 'answer' ] == chartelems[ 'answer' ] ) {
									chartparams[ k ][ 'count' ] = chartparams[ k ][ 'count' ] + chartelems[ 'count' ];
									exist = 1;
								}
							})
							if ( exist == 0 ) {
								chartparams.push( {"answer": val, "count": chartelems[ 'count' ]} );
							}
						}
					}
				})
				if ( charttype != undefined ) {
					jQuery( "#survey-results-" + random ).pmsresults({ "style": {"style": charttype, "max": 0}, "datas": [ chartparams ] });
				}
			}
		}

		jQuery( "body" ).on( "click", "#close_survey, #bglock", function(e) {
			e.preventDefault();
			if ( parseInt( survey_options[ 14 ] ) == 1 ) {
				if ( jQuery( "#bglock" ).length > 0 && ( jQuery( "#bglock" ).css( "display" ) == "block" ) && ( parseInt( survey_options[ 13 ] ) ) == 1 ) {
					jQuery( "#bglock" ).fadeOut( 1000, function() {
						jQuery( "#bglock" ).remove();
					});
				}
				if ( survey_options[ 0 ] == "bottom" ) {
					modal_survey.animate({ bottom: "-" + parseInt( modal_survey.height() + 100 ) + "px" }, parseInt( survey_options[ 11 ] ), survey_options[ 1 ], function() { modal_survey.css( "z-index", "-1" ); } )
				}
				if ( survey_options[ 0 ] == "center" ) {
					if ( survey.align == "left" ) {
						modal_survey.animate({ left: "-5000px" }, parseInt( survey_options[ 11 ] ), survey_options[ 1 ], function() { modal_survey.css( "z-index", "-1" ); } );
					}
					else {
						modal_survey.animate({ right: "-5000px" }, parseInt( survey_options[ 11 ] ), survey_options[ 1 ], function() { modal_survey.css( "z-index", "-1" ); } );
					}
				}
				if ( survey_options[ 0 ] == "top" ) {
					modal_survey.animate({ top: "-" + parseInt( modal_survey.height() + 100 ) + "px" }, parseInt( survey_options[ 11 ] ), survey_options[ 1 ], function() { modal_survey.css( "z-index", "-1" ); } )
				}
				played_question = -1;
			}
		});
			
	}
});
$.fn[ pluginName ] = function ( options ) {
		var args = arguments;
			if ( typeof options == undefined ) {
				var options = {};
			}
			if ( options === undefined || typeof options === 'object' ) {
				return this.each( function () {
					if ( ! $.data( this, 'plugin_' + pluginName ) ) {
						options.selector = this;					
						$.data( this, 'plugin_' + pluginName, new Plugin( this, options ) );
					}
				});
			} else if ( typeof options === 'string' && options[ 0 ] !== '_' && options !== 'init' ) {
				var returns;
				this.each( function () {
					var instance = $.data( this, 'plugin_' + pluginName );
					if ( instance instanceof Plugin && typeof instance[ options ] === 'function' ) {
						options.selector = this;					
						returns = instance[ options ].apply( instance, Array.prototype.slice.call( args, 1 ) );
					}
					if ( options === 'destroy' ) {
					  $.data( this, 'plugin_' + pluginName, null );
					}
				});
				return returns !== undefined ? returns : this;
			}
};
})( jQuery, window, document );