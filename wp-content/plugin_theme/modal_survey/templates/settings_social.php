	<div id="screen_preloader" style="position: absolute;width: 100%;height: 1000px;z-index: 9999;text-align: center;background: #fff;padding-top: 200px;"><h3>Modal Survey for WordPress</h3><img src="<?php print(plugins_url( '/assets/img/screen_preloader.gif' , __FILE__ ));?>"><h5><?php _e( 'LOADING', MODAL_SURVEY_TEXT_DOMAIN );?><br><br><?php _e( 'Please wait...', MODAL_SURVEY_TEXT_DOMAIN );?></h5></div>
	<div class="wrap" style="visibility:hidden">
	<br />
	<h3>Modal Survey - <?php _e( 'Social Settings', MODAL_SURVEY_TEXT_DOMAIN );?></h3>
	<div class="help_link"><a target="_blank" href="http://modalsurvey.pantherius.com/documentation/#line5"><?php _e( 'Documentation', MODAL_SURVEY_TEXT_DOMAIN );?></a></div>
	<hr />
	<?php
	if (isset($_REQUEST['settings-updated'])) {?>
	<div id="message" class="updated below-h2"><p><?php _e( 'Settings saved.', MODAL_SURVEY_TEXT_DOMAIN );?></p></div>
	<?php }?>
		<form method="post" action="options.php"> 
			<?php @settings_fields('modal_survey_social-group'); ?>
			<?php @do_settings_fields('modal_survey_social-group'); ?>
			<?php do_settings_sections('modal_survey_social'); ?>
			<?php @submit_button(); ?>
		</form>
	</div>