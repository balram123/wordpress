<?php
/*
Plugin Name: custom
*/
//wp_head(); 
// create custom plugin settings menu
add_action('admin_menu', 'custom_menu');

function custom_menu() {         
	//create new top-level menu
	//add_menu_page('Custom Form', 'Custom Form', 'edit_custom_form', __FILE__, 'custom_form' , plugins_url('/images/icon.png', __FILE__) );
        add_menu_page( 'Action Plan', 'Action Plan', 'edit_custom_form', 'my-unique-identifier', 'my_plugin_options' );
	//add_menu_page( 'Ondemand Survey', 'Ondemand Survey', 'edit_custom_form', 'my-unique-identifier1', 'modal_survey' );
	add_menu_page( 'Gyan', 'Gyan', 'edit_custom_form', 'my-unique-identifier2', 'my_plugin_options2' );	
}

/** Step 3. */
function my_plugin_options() {
	if ( !current_user_can( 'edit_custom_form' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	echo do_shortcode("[wpf-jqgrid table='wpf_jqgrid_sample' idtable=1 caption='name to display' editable=true]");
}

function my_plugin_options1() {
	if ( !current_user_can( 'edit_custom_form' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	echo '<div class="wrap">';
	echo '<p>Here is where the form would go if I actually had options.</p>';
	echo '</div>';
}

function my_plugin_options2() {
	if ( !current_user_can( 'edit_custom_form' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	echo '<div class="wrap">';
	echo '<p>Here is where the form would go if I actually had options.</p>';
	echo '</div>';
}


/**
function custom_form(){
        global $current_user;
	if($current_user->roles[0]=="author"){
            custom_form1();
        }elseif($current_user->roles[0]=="lead"){
            custom_form2();
        
        }elseif($current_user->roles[0]=="practice"){
            custom_form3();
        }else{
            custom_form1();
        }
}

function custom_form1() {

?>
<div class="wrap">
<h2>User Form</h2>

<form method="post" action="options.php">
    <?php settings_fields( 'my-cool-plugin-settings-group' ); ?>
    <?php do_settings_sections( 'my-cool-plugin-settings-group' ); ?>
    <table class="form-table">
        <tr valign="top">
        <th scope="row">User Name</th>
        <td><input type="text" name="new_option_name" value="<?php echo esc_attr( get_option('new_option_name') ); ?>" /></td>
        </tr>
         
        <tr valign="top">
        <th scope="row">User Option</th>
        <td><input type="text" name="some_other_option" value="<?php echo esc_attr( get_option('some_other_option') ); ?>" /></td>
        </tr>
        
        <tr valign="top">
        <th scope="row">User Etc.</th>
        <td><input type="text" name="option_etc" value="<?php echo esc_attr( get_option('option_etc') ); ?>" /></td>
        </tr>
    </table>    
    <?php submit_button(); ?>
</form>
</div>
<?php } 

function custom_form2() {?>
<div class="wrap">
<h2>Lead Form</h2>

<form method="post" action="options.php">
    <?php settings_fields( 'my-cool-plugin-settings-group' ); ?>
    <?php do_settings_sections( 'my-cool-plugin-settings-group' ); ?>
    <table class="form-table">
        <tr valign="top">
        <th scope="row">Lead Name</th>
        <td><input type="text" name="new_option_name" value="<?php echo esc_attr( get_option('new_option_name') ); ?>" /></td>
        </tr>
         
        <tr valign="top">
        <th scope="row">Lead Option</th>
        <td><input type="text" name="some_other_option" value="<?php echo esc_attr( get_option('some_other_option') ); ?>" /></td>
        </tr>
        
        <tr valign="top">
        <th scope="row">Lead Etc.</th>
        <td><input type="text" name="option_etc" value="<?php echo esc_attr( get_option('option_etc') ); ?>" /></td>
        </tr>
    </table>    
    <?php submit_button(); ?>
</form>
</div>
<?php } 

function custom_form3() {?>
<div class="wrap">
<h2>Practice Form</h2>

<form method="post" action="options.php">
    <?php settings_fields( 'my-cool-plugin-settings-group' ); ?>
    <?php do_settings_sections( 'my-cool-plugin-settings-group' ); ?>
    <table class="form-table">
        <tr valign="top">
        <th scope="row">Practice Name</th>
        <td><input type="text" name="new_option_name" value="<?php echo esc_attr( get_option('new_option_name') ); ?>" /></td>
        </tr>
         
        <tr valign="top">
        <th scope="row">Practice Option</th>
        <td><input type="text" name="some_other_option" value="<?php echo esc_attr( get_option('some_other_option') ); ?>" /></td>
        </tr>
        
        <tr valign="top">
        <th scope="row">Practice Etc.</th>
        <td><input type="text" name="option_etc" value="<?php echo esc_attr( get_option('option_etc') ); ?>" /></td>
        </tr>
    </table>    
    <?php submit_button(); ?>
</form>
</div>
<?php } */ ?>
