;(function ( $, window, document, undefined ) {

	"use strict";

		/** Create the defaults once **/
		var pluginName = "pmsresults",
				defaults = {
				style		 	: {},
				datas    		: []
		};
/** The actual plugin constructor **/
function Plugin ( element, options ) {
		this.element = element;
		this.settings = $.extend( {}, defaults, options );
		this._defaults = defaults;
		this._name = pluginName;
		this.init();
}

/** Avoid Plugin.prototype conflicts **/
$.extend( Plugin.prototype, {
		init: function () {
		var pieData = [], labs = [], dset = [], dset2 = [], barChartData = {}, bgcolors;
		var fillColor = '', fillColor2 = '', strokeColor = '', strokeColor2 = '', highlightFill = '', highlightFill2 = '', highlightStroke = '', highlightStroke2 = '', element = '', style = '', lng = {}, elementstyle = this.settings.style.style, counter;
		if ( this.settings.style.bgcolor == undefined ) {
			this.settings.style.bgcolor = "";
		}
		bgcolors = this.settings.style.bgcolor.split( "," );
		if ( this.settings.style.style == "progressbar" || this.settings.style.style == "linebar" ) {
			jQuery( '#' + jQuery( this.element ).attr( 'id' ) + " .survey_global_percent" ).each( function( index ) {
				element = jQuery(this);
				if ( elementstyle == "linebar" ) {
					element.css( "width", parseInt( element.closest( ".lineprocess" ).find( ".hiddenperc" ).val() ) + "%" );
					incNumbers( 0, element.closest( ".lineprocess" ).find( ".hiddenperc" ).val(), element.parent().children( ".perc" ), 16 );
				}
				else {
					element.css( "width", parseInt( element.closest( ".process" ).find( ".hiddenperc" ).val() ) + "%" );					
				}
			})
		}
		function incNumbers( start, end, jqstr, speed ) {
			var clr = null;
			var ele = jQuery( jqstr );
			var rand = start;
			var res = "";
			if ( arguments.length < 3 ) {
				throw "missing required parameters";
			}
			function loop() {
				clearTimeout( clr );
				function inloop() {
					res = rand += 1;
					ele.html( res + "%" );
					if ( !( rand < end ) ) {
						ele.html( end + "%" );
						return;
					}
					clr = setTimeout( inloop, ( speed || 32 ) );
				};
				inloop();
				//  setTimeout(loop, 2500); //Increment Loop TIme
			};
			loop();
		}
		element = this.element;
		style = this.settings.style;
		if ( this.settings.style.lng != undefined ) {
			lng = this.settings.style.lng;
		}
		else {
			lng.label1 = "";
			lng.label2 = "";		
		}
		if ( style.style == "piechart" || style.style == "doughnutchart" || style.style == "polarchart" ) {
				counter = 0;
				jQuery.each( this.settings.datas, function ( i, elem ) {
				pieData = [];
					jQuery.each( elem, function ( e, el ) {
						pieData.push({ value: el.count, color: ( bgcolors[ parseInt( counter ) + 1 ] == undefined || bgcolors[ parseInt( counter ) + 1 ] == '' ) ? get_random_color() : bgcolors[ parseInt( counter ) + 1 ], highlight: ( bgcolors[ 0 ] == undefined || bgcolors[ 0 ] == '' ) ? get_random_color() : bgcolors[ 0 ], label: el.answer })
						counter++;
					});
					if ( elementstyle == "piechart" ) {
						window.modalSurveyChart = new Chart( jQuery( "#"+jQuery( element ).attr( 'id' ) + " .modal-survey-chart" + i + " canvas" )[ 0 ].getContext( "2d" ) ).Pie( pieData );
					}
					if ( elementstyle == "doughnutchart" ) {
						window.modalSurveyChart = new Chart( jQuery( "#"+jQuery( element ).attr( 'id' ) + " .modal-survey-chart" + i + " canvas" )[ 0 ].getContext( "2d" ) ).Doughnut( pieData );
					}
					if ( elementstyle == "polarchart" ) {
						window.modalSurveyChart = new Chart( jQuery( "#"+jQuery( element ).attr( 'id' ) + " .modal-survey-chart" + i + " canvas" )[ 0 ].getContext( "2d" ) ).PolarArea( pieData );
					}
				});
		}
		if ( style.style == "barchart" || style.style == "linechart" || style.style == "radarchart" ) {
			jQuery.each( this.settings.datas, function ( i, elem ) {
			dset = [];dset2 = [];
			fillColor = ( bgcolors[ 0 ] != undefined && bgcolors[ 0 ] != '' ) ? bgcolors[ 0 ] : get_random_color(), 
			strokeColor = ( bgcolors[ 1 ] != undefined && bgcolors[ 1 ] != '' ) ? bgcolors[ 1 ] : get_random_color(), 
			highlightFill = ( bgcolors[ 2 ] != undefined && bgcolors[ 2 ] != '' ) ? bgcolors[ 2 ] : get_random_color(), 
			highlightStroke = ( bgcolors[ 3 ] != undefined && bgcolors[ 3 ] != '' ) ? bgcolors[ 3 ] : get_random_color();
			labs = [];
				jQuery.each(elem, function (e, el) {
					labs.push( el.answer );
					dset.push( el.count );
					if ( el.gcount != undefined ) {
						dset2.push( el.gcount );
					}
				});
				if ( ! jQuery.isEmptyObject( dset2 ) ) {
					if ( fillColor.substring( 0, 3 ) == 'rgb' ) {
					   highlightFill = getFromRGB( fillColor, 0.6 );
					   fillColor = getFromRGB( fillColor, 0.8 );
					} else {
					   highlightFill = getFromHex( fillColor, 0.6 );
					   fillColor = getFromHex( fillColor, 0.8 );
					}
					if ( strokeColor.substring( 0, 3 ) == 'rgb' ) {
					   strokeColor = getFromRGB( strokeColor, 0.8 );
					} else {
					   strokeColor = getFromHex( strokeColor, 0.8 );
					}
				}
				barChartData = {
						labels : labs,
						datasets : [
							{
								label: lng.label1,
								fillColor : fillColor,
								strokeColor : strokeColor,
								highlightFill: highlightFill,
								highlightStroke: strokeColor,
								pointColor: fillColor,
								pointStrokeColor: strokeColor,
								pointHighlightFill: highlightFill,
								pointHighlightStroke: strokeColor,
								data : dset
							}
						]

					}
					if ( ! jQuery.isEmptyObject( dset2 ) ) {
						fillColor2 = ( bgcolors[ 4 ] != undefined && bgcolors[ 4 ] != '' ) ? bgcolors[ 4 ] : get_random_color(), 
						strokeColor2 = ( bgcolors[ 5 ] != undefined && bgcolors[ 5 ] != '' ) ? bgcolors[ 5 ] : get_random_color(), 
						highlightFill2 = ( bgcolors[ 6 ] != undefined && bgcolors[ 7 ] != '' ) ? bgcolors[ 6 ] : get_random_color(), 
						highlightStroke2 = ( bgcolors[ 7 ] != undefined && bgcolors[ 8 ] != '' ) ? bgcolors[ 7 ] : get_random_color();
						if ( fillColor2.substring( 0, 3 ) == 'rgb' ) {
						   highlightFill2 = getFromRGB( fillColor2, 0.6 );
						   fillColor2 = getFromRGB( fillColor2, 0.8 );
						} else {
						   highlightFill2 = getFromHex( fillColor2, 0.6 );
						   fillColor2 = getFromHex( fillColor2, 0.8 );
						}
						if ( strokeColor2.substring( 0, 3 ) == 'rgb' ) {
						   strokeColor2 = getFromRGB( strokeColor2, 0.8 );
						} else {
						   strokeColor2 = getFromHex( strokeColor2, 0.8 );
						}
						barChartData.datasets.push({
								label: lng.label2,
								fillColor : fillColor2,
								strokeColor : strokeColor2,
								highlightFill: highlightFill2,
								highlightStroke: strokeColor2,
								pointColor: fillColor2,
								pointStrokeColor: strokeColor2,
								pointHighlightFill: highlightFill2,
								pointHighlightStroke: strokeColor2,
								data : dset2				
						})
					}

				if ( elementstyle == "barchart" ) {
					if ( style.max > 0 ) {
						window.modalSurveyChart = new Chart( jQuery( "#"+jQuery( element ).attr( 'id' ) + " .modal-survey-chart"+i+" canvas")[0].getContext("2d")).Bar(barChartData, {barStrokeWidth : 1, scaleOverride : true, scaleSteps : 1, scaleStepWidth : style.max, multiTooltipTemplate: "<%= datasetLabel %><%= value %>" });
					}
					else {
						window.modalSurveyChart = new Chart( jQuery( "#"+jQuery( element ).attr( 'id' ) + " .modal-survey-chart"+i+" canvas")[0].getContext("2d")).Bar(barChartData,{barStrokeWidth : 1, multiTooltipTemplate: "<%= datasetLabel %><%= value %>"});						
					}
				}
				if ( elementstyle == "linechart" ) {
					if ( style.max > 0 ) {
						window.modalSurveyChart = new Chart( jQuery( "#"+jQuery( element ).attr( 'id' ) + " .modal-survey-chart"+i+" canvas")[0].getContext("2d")).Line(barChartData, {barStrokeWidth : 1, scaleOverride : true, scaleSteps : 1, scaleStepWidth : style.max, multiTooltipTemplate: "<%= datasetLabel %><%= value %>" });
					}
					else {
						window.modalSurveyChart = new Chart( jQuery( "#"+jQuery( element ).attr( 'id' ) + " .modal-survey-chart"+i+" canvas")[0].getContext("2d")).Line(barChartData, {multiTooltipTemplate: "<%= datasetLabel %><%= value %>"});
					}
				}
				if ( elementstyle == "radarchart" ) {
					if ( style.max > 0 ) {
						window.modalSurveyChart = new Chart( jQuery( "#"+jQuery( element ).attr( 'id' ) + " .modal-survey-chart"+i+" canvas")[0].getContext("2d")).Radar(barChartData, {barStrokeWidth : 1, scaleOverride : true, scaleSteps : 1, scaleStepWidth : style.max, multiTooltipTemplate: "<%= datasetLabel %><%= value %>" });
					}
					else {
						window.modalSurveyChart = new Chart( jQuery( "#"+jQuery( element ).attr( 'id' ) + " .modal-survey-chart"+i+" canvas")[0].getContext("2d")).Radar(barChartData, {multiTooltipTemplate: "<%= datasetLabel %><%= value %>"});
					}
				}
			});
		}
		function getFromRGB( color, transparency ) 
		{
			if ( color.indexOf( 'a' ) == -1 ){
				var result = color.replace( ')', ', ' + transparency + ' )').replace( 'rgb', 'rgba' );
			}
			return result;
		}

		function getFromHex( color, transparency ) 
		{ 
			var patt = /^#([\da-fA-F]{2})([\da-fA-F]{2})([\da-fA-F]{2})$/;
			var matches = patt.exec( color );
			var rgba = "rgba(" + parseInt( matches[ 1 ], 16 ) + "," + parseInt( matches[ 2 ], 16 ) + "," + parseInt( matches[ 3 ], 16 ) + ", " + transparency + " )";
			return rgba;
		}			
		function get_random_color() {
			var letters = '0123456789ABCDEF'.split('');
			var color = '#';
			for (var i = 0; i < 6; i++ ) {
				color += letters[Math.round(Math.random() * 15)];
			}
			return color;
		}
	}
});
		$.fn[ pluginName ] = function ( options ) {
				return this.each(function() {
						if ( !$.data( this, "plugin_" + pluginName ) ) {
								$.data( this, "plugin_" + pluginName, new Plugin( this, options ) );
						}
				});
		};
})( jQuery, window, document );