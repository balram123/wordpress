<?php
function action_lists () {
?>

<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/action-tables/style-admin.css" rel="stylesheet" />
<link type="text/css" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css" rel="stylesheet" />
<div class="wrap">
<h2>Action Lists</h2>

<?php
global $wpdb;
$table_name = $wpdb->prefix . 'action_table';

if ( is_admin() ) {
  $rows = $wpdb->get_results("SELECT * from $table_name");
} else {
  $current_user = wp_get_current_user();
  $rows = $wpdb->get_results($wpdb->prepare("SELECT * from $table_name where created_by = %s", $current_user->ID));
}

$arr = (array)$rows;
if (!empty($arr)) {
    // do stuff
	//print_r($arr);
	$main_array = array();
	foreach ( $rows as $key => $value ) {
		$data_array = array();
		$data_array['id'] = "<a target='_blank' href='".admin_url('admin.php?page=action_list_update&id='.$value->id)."'>Edit</a>";
		$data_array['initiative'] = ($value->initiative == '' ? ' - ' : $value->initiative);
		$data_array['workstream'] = ($value->workstream == '' ? ' - ' : $value->workstream);
		$data_array['initiative_success'] = ($value->initiative_success == '' ? ' - ' : $value->initiative_success);
		$data_array['Budject'] = ($value->budget == '' ? ' - ' : $value->budget);
		array_push($main_array, $data_array);
	}
	//print_r ($main_array);
}
else{
	echo '<h1> No Records found.</h1>';
}

	

?>
<table id="data_table" class="display" width="100%"></table>

</div>
<script>
	var dataSet = <?php echo json_encode($main_array);?>;
	console.log(dataSet);
</script>
<?php
}