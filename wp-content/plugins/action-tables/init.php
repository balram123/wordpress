<?php
/*
Plugin Name: Action Lists
Description: Plugin from comp
Version: 1.0
Author: kk
Author URI: 
*/

//menu items
add_action('admin_menu','action_table_menu');
function action_table_menu() {
	
	//this is the main item for the menu
	add_menu_page('Action Tables', //page title
	'Action Table', //menu title
	'manage_options', //capabilities
	'action_lists', //menu slug
	'action_lists' //function
	);
	
	//this is a submenu
	add_submenu_page('action_lists', //parent slug
	'Add New Action Table', //page title
	'Add New', //menu title
	'manage_options', //capability
	'action_list_create', //menu slug
	'action_list_create'); //function
	
	//this submenu is HIDDEN, however, we need to add it anyways
	add_submenu_page(null, //parent slug
	'Update Action List', //page title
	'Update', //menu title
	'manage_options', //capability
	'action_list_update', //menu slug
	'action_list_update'); //function
}

register_activation_hook( __FILE__, 'create_table' );

function create_table() {

	global $wpdb;
	$charset_collate = $wpdb->get_charset_collate();
	$table_name = $wpdb->prefix . 'action_table';
//array( 'initiative' => $initiative,'workstream' => $workstream, 'workstream_team' => $workstream_team, 'initiative_team' => $initiative_team, 'initiative_status' => $initiative_status, 'what_i_need_from' => $what_i_need_from, 'wave' => $wave, 'initiative_success' => $initiative_success, 'start_date' => $start_date, 'close_date' => $close_date, 'budget' => $budget, 'linkage' => $linkage )
	$sql = "CREATE TABLE $table_name (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		initiative varchar(255),
		workstream varchar(255),
		workstream_team varchar(255),
		initiative_team varchar(255),
		initiative_status varchar(255),
		what_i_need_from varchar(255),
		wave varchar(255),
		initiative_success varchar(255),
		start_date datetime NULL DEFAULT NULL,
		close_date datetime NULL DEFAULT NULL,
		budget varchar(255),
		linkage varchar(255),
		created_by int(11),
		created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
		UNIQUE KEY id (id)
	) $charset_collate;";
	

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
}

define('ROOTDIR', plugin_dir_path(__FILE__));
function actiontables_enqueue_scripts() { 

 wp_register_style( 'action-tables-style', plugins_url('/style-admin.css', __FILE__) );
 wp_enqueue_style( 'action-tables-style' );
 wp_register_script( 'action-tables-script', 'https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js' );
 wp_enqueue_script( 'action-tables-script' );
 
 wp_register_script( 'action-tables-custom-script', WP_PLUGIN_URL.'/action-tables/scripts.js' );
 wp_enqueue_script( 'action-tables-custom-script' );

 }
add_action('admin_head', 'actiontables_enqueue_scripts');

require_once(ROOTDIR . 'action-list.php');
require_once(ROOTDIR . 'action-list-create.php');
require_once(ROOTDIR . 'action-list-update.php');
