jQuery(document).ready(function() {
    
    

    jQuery('#data_table').DataTable( {
        aaData: dataSet,
        columns: [
            { title: "Budject", data: "Budject"},
            { title: "Initiative", data: "initiative" },
            { title: "Workstream", data: "workstream" },
            { title: "initiative_success", data: "initiative_success" }
        ],
         "columnDefs": [
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    //console.log(row.id);
                    return row.id;
                },
                "targets": 4
            }
        ]
    } );
} );