<?php

function action_list_create() {
	$initiative		 = $_POST[ "initiative" ];
	$workstream	 = $_POST[ "workstream" ];
	$workstream_team	 = $_POST[ "workstream_team" ];
	$initiative_team	 = $_POST[ "initiative_team" ];
	$initiative_status	 = $_POST[ "initiative_status" ];
	$what_i_need_from	 = $_POST[ "what_i_need_from" ];
	$wave	 = $_POST[ "wave" ];
	$initiative_success	 = $_POST[ "initiative_success" ];
	$start_date	 = $_POST[ "start_date" ];
	$close_date	 = $_POST[ "close_date" ];
	$budget	 = $_POST[ "budget" ];
	$linkage = $_POST[ "linkage" ];
	$current_user = wp_get_current_user();
	$created_by = $current_user->ID;

//insert
	if ( isset( $_POST[ 'insert' ] ) ) {
		global $wpdb;
		$table_name = $wpdb->prefix . 'action_table';
		
		$wpdb->insert(
		$table_name, //table
  array( 'initiative' => $initiative,'workstream' => $workstream, 'workstream_team' => $workstream_team, 'initiative_team' => $initiative_team, 'initiative_status' => $initiative_status, 'what_i_need_from' => $what_i_need_from, 'wave' => $wave, 'initiative_success' => $initiative_success, 'start_date' => $start_date, 'close_date' => $close_date, 'budget' => $budget, 'linkage' => $linkage,'created_by' => $created_by, 'created_at' => current_time('mysql', 1)), //data
  array( '%s', '%s','%s', '%s','%s', '%s','%s', '%s','', '','%s', '%s', '%s') //data format			
		);
		$message = '';
		$message.="Action Table data inserted";
	}
	?>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous" />
	<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/action-tables/style-admin.css" rel="stylesheet" />
	<div class="wrap">
	<?php if ( isset( $message ) ): ?><div class="updated"><p><?php echo $message; ?></p></div><?php endif; ?>
	<form class="form-action-list" method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>">
			
				

					<!-- Form Name -->
					<legend>Add New Action Details</legend>
				<fieldset>
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-4 control-label" for="initiative">Initiative #</label>
						<div class="col-sm-6">
							<input type="text" name="initiative" placeholder="Initiative #" value="<?php echo $initiative; ?>" class="form-control">
						</div>
					</div>
				</fieldset>
				<fieldset>
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-4 control-label" for="workstream">Focus Area/Workstream</label>
						<div class="col-sm-6">
							<input type="text"  name="workstream" placeholder="Focus Area/Workstream" value="<?php echo $workstream; ?>"  class="form-control">
						</div>
					</div>
				</fieldset>
				<fieldset>
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-4 control-label" for="workstream_team">Focus Area/Workstream Team</label>
						<div class="col-sm-6">
							<input type="text" name="workstream_team" placeholder="Focus Area/Workstream Team" value="<?php echo $workstream_team; ?>" class="form-control">
						</div>
					</div>
				</fieldset>
				<fieldset>
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-4 control-label" for="initiative_team">Initiative Name</label>
						<div class="col-sm-6">
							<input type="text" name="initiative_team" placeholder="Initiative Name" value="<?php echo $initiative_team; ?>" class="form-control">
						</div>
				</fieldset>
				<fieldset>
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-4 control-label" for="initiative_status">Initiative Name</label>
						<div class="col-sm-6">
							<input type="text" name="initiative_status" placeholder="Initiative Status" value="<?php echo $initiative_status; ?>" class="form-control">
						</div>
				</fieldset>
			<fieldset>
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-4 control-label" for="what_i_need_from">What I need from Cognizant</label>
						<div class="col-sm-6">
							<input type="text" name="what_i_need_from" placeholder="What I need from Cognizant" value="<?php echo $what_i_need_from; ?>" class="form-control">
						</div>
				</fieldset>
					<fieldset>
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-4 control-label" for="wave">Wave # (1, 2, 3,4)</label>
						<div class="col-sm-6">
							<input type="text" name="wave" placeholder="Wave # (1, 2, 3,4)" value="<?php echo $wave; ?>" class="form-control">
						</div>
				</fieldset>
					<fieldset>
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-4 control-label" for="initiative_success">Initiative - Indicators of Success</label>
						<div class="col-sm-6">
							<input type="text" name="initiative_success" placeholder="Initiative - Indicators of Success" value="<?php echo $initiative_success; ?>" class="form-control">
						</div>
				</fieldset>
					<fieldset>
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-4 control-label" for="start_date">Target Start Date</label>
						<div class="col-sm-6">
							<input type="date" name="start_date" placeholder="Target Start Date" value="<?php echo $start_date; ?>" class="form-control">
						</div>
				</fieldset>
					<fieldset>
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-4 control-label" for="close_date">Target Close Date</label>
						<div class="col-sm-6">
							<input type="date" name="close_date" placeholder="Target Close Date" value="<?php echo $close_date; ?>" class="form-control">
						</div>
				</fieldset>
					<fieldset>
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-4 control-label" for="budget">Budget (if applicable) (USD)</label>
						<div class="col-sm-6">
							<input type="text" name="budget" placeholder="Budget (if applicable) (USD)" value="<?php echo $budget; ?>" class="form-control">
						</div>
				</fieldset>
					<fieldset>
					<!-- Text input-->
					<div class="form-group">
						<label class="col-sm-4 control-label" for="linkage">Linkage to other initiatives (if any) (within/across Work Streams)</label>
						<div class="col-sm-6">
							<input type="text" name="linkage" placeholder="Linkage to other initiatives (if any) (within/across Work Streams)" value="<?php echo $linkage; ?>" class="form-control">
						</div>
				</fieldset>
	
	<input type='submit' name="insert" value='Save' class='button'>
	</form>
	<div class="clear"></div>
	</div>
	<?php
}
