<?php
/**
 * @package iconnect-ey
 * @version 1.6
 */
/*
Plugin Name: Iconnect-ey
Plugin URI: #
Description: This is customplugin,
Version: 1.0
Author URI: #
*/


// to avoid searching pages and posts
function SearchFilter($query) {
if ($query->is_search) {
$query->set('post_type', '');
}
return $query;
}

add_filter('pre_get_posts','SearchFilter');

add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );

function remove_wp_logo( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'wp-logo' );
}

/*Update worpress login logo and url*/

function enchanting_login_logo() { ?>
    <style type="text/css">
        .login h1 a {
            background-color: #CFC388;
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.gif);
            background-size: 100% 100%;
            height: auto;
            padding: 10px 10px 10px 10px;
            width: 165px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'enchanting_login_logo' );

add_filter( 'login_headerurl', 'enchanting_loginlogo_url' );
function enchanting_loginlogo_url($url) {
    return  site_url();
}


//restrict admin access
add_action( 'init', 'blockusers_init' );
function blockusers_init() {
if ( is_admin() && !current_user_can( 'manage_options' ) &&
! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
	wp_redirect( home_url() );
	exit;
}
}

//hide dashboard from front-end
add_filter('show_admin_bar', '__return_false');


function mypo_parse_query_useronly( $wp_query ) {
    if ( strpos( $_SERVER[ 'REQUEST_URI' ], '/wp-admin/edit.php' ) !== false ) {
        if ( !current_user_can( 'update_core' ) ) {
            global $current_user;
            $wp_query->set( 'author', $current_user->id );
        }
    }
}

add_filter('parse_query', 'mypo_parse_query_useronly' );

function custom_menu_page_removing() {
    remove_menu_page( 'profile.php' );
}
add_action( 'admin_menu', 'custom_menu_page_removing' );

function wpdocs_dequeue_script() {
   wp_deregister_script( 'modal_survey_script' );
   wp_enqueue_script('modal_survey_script', site_url().'/wp-content/plugins/iconnect-ey/js/modal_survey.js', array( 'jquery' ), '1.1' );
}
add_action( 'wp_print_scripts', 'wpdocs_dequeue_script', 100 );